# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151206091743) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answeredquestions", force: :cascade do |t|
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "name"
    t.date     "birthday"
    t.string   "gender"
    t.string   "sport"
    t.string   "othersport"
    t.string   "reason"
    t.string   "frequency"
    t.datetime "answered_at"
    t.string   "email"
    t.datetime "addedemail_at"
    t.string   "questionkey"
  end

  create_table "attendants", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.string   "attend"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "attendants", ["event_id"], name: "index_attendants_on_event_id", using: :btree
  add_index "attendants", ["user_id"], name: "index_attendants_on_user_id", using: :btree

  create_table "authorizations", force: :cascade do |t|
    t.string   "provider"
    t.string   "uid"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "email"
  end

  add_index "authorizations", ["email"], name: "index_authorizations_on_email", using: :btree

  create_table "cmsusers", force: :cascade do |t|
    t.string   "username"
    t.string   "password_digest"
    t.string   "full_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "role"
    t.string   "uuid"
    t.integer  "status"
  end

  add_index "cmsusers", ["uuid"], name: "index_cmsusers_on_uuid", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "post_id"
    t.integer  "user_id"
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "available"
  end

  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "venue"
    t.datetime "eventtime"
    t.boolean  "available"
    t.integer  "sport_id"
    t.integer  "user_id"
    t.string   "privacy"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "events", ["sport_id"], name: "index_events_on_sport_id", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "following_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string   "name"
    t.string   "detail"
    t.integer  "sport_id"
    t.integer  "user_id"
    t.string   "privacy"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "group_type"
  end

  add_index "groups", ["sport_id"], name: "index_groups_on_sport_id", using: :btree
  add_index "groups", ["user_id"], name: "index_groups_on_user_id", using: :btree

  create_table "memberships", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "user_id"
    t.string   "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "status"
  end

  add_index "memberships", ["group_id"], name: "index_memberships_on_group_id", using: :btree
  add_index "memberships", ["user_id"], name: "index_memberships_on_user_id", using: :btree

  create_table "mobile_phones", force: :cascade do |t|
    t.string   "mobileno"
    t.string   "pin"
    t.datetime "expired_at"
    t.boolean  "verified"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "message"
    t.string   "posttype"
    t.string   "targettype"
    t.string   "targetid"
    t.string   "attachment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "sport_id"
    t.boolean  "available"
  end

  add_index "posts", ["sport_id"], name: "index_posts_on_sport_id", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "sports", force: :cascade do |t|
    t.string   "name"
    t.string   "detail"
    t.integer  "sort"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "fileBg_file_name"
    t.string   "fileBg_content_type"
    t.integer  "fileBg_file_size"
    t.datetime "fileBg_updated_at"
    t.string   "fileCover_file_name"
    t.string   "fileCover_content_type"
    t.integer  "fileCover_file_size"
    t.datetime "fileCover_updated_at"
    t.string   "fileIcon_file_name"
    t.string   "fileIcon_content_type"
    t.integer  "fileIcon_file_size"
    t.datetime "fileIcon_updated_at"
  end

  create_table "sportsprefs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "sport_id"
    t.string   "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "sportsprefs", ["sport_id"], name: "index_sportsprefs_on_sport_id", using: :btree
  add_index "sportsprefs", ["user_id"], name: "index_sportsprefs_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "full_name"
    t.string   "email"
    t.string   "password_digest"
    t.string   "accesskey"
    t.datetime "last_login"
    t.string   "uuid"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "fileCover_file_name"
    t.string   "fileCover_content_type"
    t.integer  "fileCover_file_size"
    t.datetime "fileCover_updated_at"
    t.string   "username"
    t.string   "status"
    t.string   "mobileno"
  end

  add_index "users", ["username"], name: "index_users_on_username", using: :btree

  add_foreign_key "attendants", "events"
  add_foreign_key "attendants", "users"
  add_foreign_key "comments", "posts"
  add_foreign_key "comments", "users"
  add_foreign_key "events", "sports"
  add_foreign_key "events", "users"
  add_foreign_key "groups", "sports"
  add_foreign_key "groups", "users"
  add_foreign_key "memberships", "groups"
  add_foreign_key "memberships", "users"
  add_foreign_key "posts", "users"
  add_foreign_key "sportsprefs", "sports"
  add_foreign_key "sportsprefs", "users"
end
