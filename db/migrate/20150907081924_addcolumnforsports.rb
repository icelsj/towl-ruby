class Addcolumnforsports < ActiveRecord::Migration

  def up
    add_attachment :sports, :fileBg
    add_attachment :sports, :fileCover
    add_attachment :sports, :fileIcon
  end
  def down
    remove_attachment :sports, :fileBg
    remove_attachment :sports, :fileCover
    remove_attachment :sports, :fileIcon
  end
end
