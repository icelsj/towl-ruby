class Removetypefromgroups < ActiveRecord::Migration
  def change
    remove_column :groups, :type
    add_column :groups, :group_type, :string
  end
end
