class AddEmailForAutho < ActiveRecord::Migration
  def change
    add_column :authorizations, :email, :string
    add_index :authorizations, :email
  end
end
