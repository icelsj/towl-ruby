class CreateAttendants < ActiveRecord::Migration
  def change
    create_table :attendants do |t|
      t.references :user, index: true, foreign_key: true
      t.references :event, index: true, foreign_key: true
      t.string :attend

      t.timestamps null: false
    end
  end
end
