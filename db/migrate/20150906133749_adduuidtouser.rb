class Adduuidtouser < ActiveRecord::Migration
  def change
    add_column :users, :uuid, :string
    add_column :users, :status, :integer
  end
end
