class Addsportstopost < ActiveRecord::Migration
  def change
    add_reference :posts, :sport, index: true
  end
end
