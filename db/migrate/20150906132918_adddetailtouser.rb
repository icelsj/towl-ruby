class Adddetailtouser < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :full_name, :string
    add_column :users, :email, :string
    add_column :users, :password_digest, :string
    add_column :users, :facebook_id, :string
    add_column :users, :accesskey, :string
    add_column :users, :last_login, :datetime
  end
end
