class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.string :venue
      t.datetime :eventtime
      t.boolean :available
      t.references :sport, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.string :privacy

      t.timestamps null: false
    end
  end
end
