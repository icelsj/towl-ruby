class Adddetailtocmsuser < ActiveRecord::Migration
  def change
    add_column :cmsusers, :role, :integer
    add_column :cmsusers, :uuid, :string
    add_column :cmsusers, :status, :integer
    add_index :cmsusers, :uuid
  end
end
