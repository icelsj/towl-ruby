class RemoveSports < ActiveRecord::Migration
  def change
    remove_column :sports, :imgBg
    remove_column :sports, :imgCover
    remove_column :sports, :imgIcon

  end

  def self.up
    add_attachment :sports, :fileBg
    add_attachment :sports, :fileCover
    add_attachment :sports, :fileIcon
  end
  def self.down
    remove_attachment :sports, :fileBg
    remove_attachment :sports, :fileCover
    remove_attachment :sports, :fileIcon
  end
end
