class CreateSports < ActiveRecord::Migration
  def change
    create_table :sports do |t|
      t.string :name
      t.string :detail
      t.string :imgBg
      t.string :imgCover
      t.string :imgIcon
      t.integer :sort

      t.timestamps null: false
    end
  end
end
