class AddUserAvatar < ActiveRecord::Migration
  def up
    add_attachment :users, :avatar
    add_attachment :users, :fileCover
  end
  def down
    remove_attachment :users, :avatar
    remove_attachment :users, :fileCover
  end
end
