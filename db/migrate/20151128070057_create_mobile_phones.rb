class CreateMobilePhones < ActiveRecord::Migration
  def change
    create_table :mobile_phones do |t|
      t.string :mobileno
      t.string :pin
      t.datetime :expired_at
      t.boolean :verified

      t.timestamps null: false
    end
  end
end
