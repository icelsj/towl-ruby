class AddFieldToQuestion < ActiveRecord::Migration
  def change
    add_column :answeredquestions, :name, :string
    add_column :answeredquestions, :birthday, :date
    add_column :answeredquestions, :gender, :string
    add_column :answeredquestions, :sport, :string
    add_column :answeredquestions, :othersport, :string
    add_column :answeredquestions, :reason, :string
    add_column :answeredquestions, :frequency, :string
    add_column :answeredquestions, :answered_at, :datetime
    add_column :answeredquestions, :email, :string
    add_column :answeredquestions, :addedemail_at, :datetime
  end
end
