class AddHideToComment < ActiveRecord::Migration
  def change
    add_column :comments, :available, :boolean
  end
end
