class CreateCmsusers < ActiveRecord::Migration
  def change
    create_table :cmsusers do |t|
      t.string :username
      t.string :password_digest
      t.string :full_name

      t.timestamps null: false
    end
  end
end
