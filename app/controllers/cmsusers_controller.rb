class CmsusersController < ApplicationController
  # GET /admin_signup
  def new
    @cmsuser = Cmsuser.new
  end

  # POST /admin_signup
  def create
    @cmsuser = Cmsuser.new(cmsuser_params)

    respond_to do |format|
      if @cmsuser.save
        format.html { redirect_to root_path, notice: 'Admin account was successfully created.' }
        format.json { render root_path, status: :created, location: @cmsuser }
      else
        format.html { render :new }
        format.json { render json: @cmsuser.errors, status: :unprocessable_entity }
      end
    end
  end

private
  # Never trust parameters from the scary internet, only allow the white list through.
  def cmsuser_params
    params.require(:cmsuser).permit(:full_name, :username, :password)
  end
end
