class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  # skip_before_action :verify_authenticity_token

  helper_method :current_user

  def current_user
    @current_user ||= Cmsuser.where(uuid: session[:admin_id]) if session[:admin_id]
  end

  def require_user
    redirect_to '/login' unless current_user
  end

end
