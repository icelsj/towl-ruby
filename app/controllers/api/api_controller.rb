require 'rack/auth/digest/md5'

class Api::ApiController < ApplicationController
  respond_to :json
  protect_from_forgery with: :null_session

private
  # Authentication and other filters implementation.
  def authenticate
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey

    unless @user
      respond_to do |format|
        format.json { render :json => { :success => 0, :message => 'unauthorized', :code => "99001" },status: :unauthorized }
      end
      return false
    end
  end

  def authenticate_hash
    accesskey = request.headers['X-Check-Key']
    checkkey = request.headers['X-Sum-Key']

    saltkey = 'towl.'.concat(accesskey.to_s.concat('.attspace'))
    checksum = Digest::MD5.hexdigest(saltkey)

    unless checkkey == checksum
      respond_to do |format|
        format.json { render :json => { :success => 0, :message => 'unauthorized', :code => "99001" }, status: :unauthorized }
      end
      # render json: { :success => 0, :message => 'unauthorized' }, status: :unauthorized
      return false
    end
  end
end