class Api::V1::CommentsController < Api::ApiController
  respond_to :json

  before_action :authenticate, only: [:create, :destroy]

  def create
    accesskey = request.headers['X-Access-Key']
    post_id = params[:post_id]
    @user = User.where(accesskey: accesskey).first if accesskey
    @comment = Comment.new(comment_params)

    respond_with do |format|
      if @user
        @comment.post_id = post_id
        @comment.user_id = @user.id
        if @comment.save
          # return data
          format.json { render :json => { :success => true, :data => @comment, :message => 'Your comment is posted successfully.', :code => "8001" }, status: :created }
        else
          format.json { render json: { :success => false, :data => @comment.errors, :message => 'Failed to post comment.', :code => "8002" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "8003" }, status: :unauthorized }
      end
    end
  end

  def destroy
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @comment = Comment.find(params[:id])
    @post = Post.find(@comment.post_id) if @comment
    isadmin = false

    if @post.targettype.eql? "group"
      @membership = Membership.where(group_id:@post.targetid, user_id:@user.id) if @user
      if @membership && @membership.count >= 0 && @membership.first.role.eql?("moderator")
        isadmin = true
      end
    end

    respond_with do |format|
      if @user && (isadmin || @user.id == @comment.user_id || @user.id == @post.user_id)
        if @comment.update :available => false
          # return data
          format.json { render :json => { :success => true, :data =>"", :message => 'Your comment is removed successfully.', :code => "8005" }, status: :created }
        else
          format.json { render json: { :success => false, :data => @comment.errors, :message => 'Failed to remove comment.', :code => "8006" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "8004" }, status: :unauthorized }
      end
    end
  end

  def postcomments
    post_id = params[:post_id]
    params[:page] = 1 unless params.has_key?(:page)

    respond_with do |format|

      @comment = Comment.where(post_id:post_id, available:true).order('created_at desc')
      @post_entries = @comment.page(params[:page])
      @absolute_url = request.host
      if request.host.eql?('localhost') then
        @absolute_url = request.host_with_port
      end
      @absolute_url = @absolute_url.concat('/api/v1/posts/').concat(post_id).concat('/comments/page/')
      @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
      @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @post_entries.total_pages
      format.json { render :json => { :success => true,
                                      :total_items => @comment.count,
                                      :total_pages => @post_entries.total_pages,
                                      :prev_url => @prev_url,
                                      :next_url => @next_url,
                                      :data => @post_entries,
                                      :message => 'Post comments listing successfully.',
                                      :code => "8007" },
                           status: :ok }

    end
  end
private
  # Never trust parameters from the scary internet, only allow the white list through.
  def comment_params
    params.require(:comment).permit(:message)
  end
end
