class Api::V1::AttendantsController < Api::ApiController
  respond_to :json

  before_action :authenticate

  def invitation_response
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @event = Event.find(params[:id])

    @attendant = Attendant.where(user_id: @user.id, event_id: @event.id).first

    respond_with do |format|
      if ['going','decline','maybe'].include? params[:action_name].to_s
        if !@attendant
          @attendant = Attendant.new :user_id => @user.id, :event_id => @event.id, :attend => params[:action_name].to_s
        else
          @attendant.attend = params[:action_name].to_s
        end

        if @attendant.save
          format.json { render :json => { :success => true, :data => @attendant, :message => 'Your have respnded to the event.', :code => "10002" }, status: :created }
        else
          format.json { render json: { :success => false, :data => @attendant.errors, :message => 'Failed to respond to the event.', :code => "10003" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid event action', :code => "10001" }, status: :unprocessable_entity }
      end
    end
  end

end
