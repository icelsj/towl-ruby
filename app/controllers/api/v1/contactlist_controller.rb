class Api::V1::ContactlistController < Api::ApiController
  respond_to :json

  before_action :authenticate

  def searchfriends
    #
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    contacts = params[:contacts]
    @found_contact = []
    respond_with do |format|
      if @user
        for i in 0..contacts.count - 1
          mobileno = contacts[i][:contact] if contacts[i]

          mobileno.each do |m|
            @found = User.find_by_mobileno m
            @found_contact.push @found if @found
          end
          format.json { render :json => { :success => true, :data => @found_contact, :message => 'found friends.', :code => "11002" }, :except => [:accesskey, :password_digest], status: :ok }
        end
      else
        format.json { render :json => { :success => false, :data => "", :message => 'Unauthorized.', :code => "11001" }, status: :unauthorized }
      end
    end
  end
end
