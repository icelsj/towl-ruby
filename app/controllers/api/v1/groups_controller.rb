class Api::V1::GroupsController < Api::ApiController
  respond_to :json

  before_action :authenticate, only: [:create, :update, :joined]

  # GET /groups
  def index
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    userid = @user.id if @user

    params[:page] = 1 unless params.has_key?(:page)
    sport_id = params[:sport_id] if params[:sport_id]
    if sport_id
      @group = Group.where(sport_id:sport_id).where.not(privacy:"private")
    else
      @group = Group.where.not(privacy:"private")
    end

    @group_entries = @group.page(params[:page])
    @absolute_url = request.host
    if request.host.eql?('localhost') then
      @absolute_url = request.host_with_port
    end
    if sport_id
      @absolute_url = @absolute_url.concat('/api/v1/groups/sports/').concat(sport_id.to_s).concat('/page/')
    else
      @absolute_url = @absolute_url.concat('/api/v1/groups/page/')
    end
    @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
    @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @group_entries.total_pages

    respond_with do |format|
      format.json { render :json => { :success => true,
                                      :total_items => @group.count,
                                      :total_pages => @group_entries.total_pages,
                                      :prev_url => @prev_url,
                                      :next_url => @next_url,
                                      :data => @group_entries.as_json(user_id:userid),
                                      :code => "5001",
                                      :message => 'List group successfully.'
                           }
      }
    end
  end

  # POST /groups
  def create
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @group = Group.new(group_params)

    respond_with do |format|
      if @user
        @group.user_id = @user.id
        if @group.save
          # create a membership
          @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
          @membership.save

          # return data
          format.json { render :json => { :success => true, :data => @group, :message => 'Your group is created successfully.', :code => "5002" }, status: :created }
        else
          format.json { render json: { :success => false, :data => @group.errors, :message => 'Failed to create a group.', :code => "5003" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :data => @group.errors, :message => 'Invalid user owner.', :code => "5004" }, status: :unauthorized }
      end
    end
  end

  # GET /groups/1
  def show
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    userid = @user.id if @user

    @group = Group.find(params[:id])
    respond_with do |format|
      if @group
        format.json { render :json => { :success => true, :data => @group.as_json(user_id:userid), :message => 'Found group', :code => "5005" }, status: :ok}
      else
        format.json { render :json => { :success => false, :message => 'not found', :code => "5006" }, status: :forbidden }
      end
    end
  end

  # PATCH/PUT /groups/1
  def update
    @group = Group.find(params[:id])
    respond_with do |format|
      if @group
        accesskey = request.headers['X-Access-Key']
        @user = User.where(accesskey: accesskey, id:@group.user_id).first if accesskey

        if !@user
          format.json { render json: { :success => false, :data => @group.errors, :message => 'Failed to update a group that is not belong to user.', :code => "5004" }, status: :unauthorized }
          return false
        end

        @group.update(group_params)
        if @group.save
          format.json { render :json => { :success => true, :data => @group, :message => 'Your group is updated successfully.', :code => "5007" }, status: :ok }
        else
          format.json { render json: { :success => false, :data => @group.errors, :message => 'Failed to update a group.', :code => "5008" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :data => @group.errors, :message => 'Invalid group.', :code => "5006" }, status: :unprocessable_entity }
      end
    end
  end

  # Joined
  def joined
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    userid = @user.id if @user

    params[:page] = 1 unless params.has_key?(:page)
    sport_id = params[:sport_id] if params[:sport_id]

    # @membership = Membership.where(user_id:userid, status:"joined")

    if sport_id
      @group = Group.joins(:memberships).where(sport_id:sport_id, memberships: { user_id:userid, status: "joined" })
    else
      @group = Group.joins(:memberships).where(memberships: { user_id:userid, status: "joined" })
    end

    @group_entries = @group.page(params[:page])
    @absolute_url = request.host
    if request.host.eql?('localhost') then
      @absolute_url = request.host_with_port
    end
    if sport_id
      @absolute_url = @absolute_url.concat('/api/v1/groups/joined/sports/').concat(sport_id.to_s).concat('/page/')
    else
      @absolute_url = @absolute_url.concat('/api/v1/groups/joined/page/')
    end
    @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
    @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @group_entries.total_pages

    respond_with do |format|
      format.json { render :json => { :success => true,
                                      :total_items => @group.count,
                                      :total_pages => @group_entries.total_pages,
                                      :prev_url => @prev_url,
                                      :next_url => @next_url,
                                      :data => @group_entries.as_json(user_id:userid),
                                      :code => "5001",
                                      :message => 'List group successfully.' } }
    end
  end

private
  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params.require(:group).permit(:name, :detail, :sport_id, :group_type, :privacy)
  end
end
