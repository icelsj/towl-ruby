class Api::V1::MembershipsController < Api::ApiController
  respond_to :json

  before_action :authenticate

  def create
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey, username: params[:username]).first if accesskey
    @group = Group.find(params[:group_id])

    @membership = Membership.new
    @membership.user_id = @user.id
    @membership.group_id = @group.id
    @membership.role = 'member'
    if @group.privacy.downcase.eql?('closed')
      @membership.status = 'pending'
    else
      @membership.status = 'joined'
    end


    # @membership.status = 'joined'
    @checkmembership = Membership.where(user_id: @user.id, group_id: @group.id).first

    respond_with do |format|
      if @checkmembership
        format.json { render :json => { :success => false, :message => 'You already joined this group.', :code => "6001" }, status: :unprocessable_entity }
      elsif @user
        @membership.user_id = @user.id
        if @membership.save
          format.json { render :json => { :success => true, :data => @membership, :message => 'Your has joined a group successfully.', :code => "6002" }, status: :created }
        else
          format.json { render json: { :success => false, :data => @membership.errors, :message => 'Failed to join a group.', :code => "6003" }, status: :unprocessable_entity }
        end
      end
    end
  end

  def destroy
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey, username: params[:username]).first if accesskey
    @membership = Membership.where(user_id: @user.id, group_id: params[:group_id]).first

    respond_with do |format|
      if @user && @membership
        @membership.user_id = @user.id
        if @membership.destroy
          format.json { render :json => { :success => true, :data =>"", :message => 'Your has leaved a group successfully.', :code => "6004" }, status: :created }
        else
          format.json { render json: { :success => false, :message => 'Failed to leave a group.', :code => "6005" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'You have not joined this group yet.', :code => "6006" }, status: :unprocessable_entity }
      end
    end
  end

  # GET /groups/1/request
  def getrequest
    group_id = params[:id]
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @usermember = Membership.where(group_id: group_id, user_id: @user.id).first
    @membership = Membership.where(group_id: group_id, status: 'pending')

    respond_with do |format|
      if @usermember.role.eql?('moderator')
        format.json { render :json => { :success => true, :data => @membership.as_json(showInGroup:true), :message => 'Group join request list successfully', :code => "6007" }, status: :ok }
      else
        format.json { render :json => { :success => false, :message => 'Group join request listing is only for group moderator.', :code => "6008" }, status: :unauthorized }
      end
    end
  end

  # GET /groups/1/request
  def approverequest
    group_id = params[:id]
    action_name = params[:action_name].to_s.downcase
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @usermember = Membership.where(group_id: group_id, user_id: @user.id).first
    @membership = Membership.find(params[:request_id])

    respond_with do |format|
      if @usermember.role.eql?('moderator')
        case action_name
          when 'approve'
            @membership.status = 'joined'
          when 'pending'
            @membership.status = 'pending'
        end
        if @membership.save
          format.json { render :json => { :success => true, :data => @membership, :message => 'Group join request status successfully', :code => "6009" }, status: :ok }
        else
          format.json { render :json => { :success => false, :data => @membership.errors, :message => 'Group join request status failed', :code => "6010" }, status: :ok }
        end
      else
        format.json { render :json => { :success => false, :message => 'Group join request status is only for group moderator.', :code => "6008" }, status: :unauthorized }
      end
    end
  end

  # GET /memberships/1/promote
  def promote
    membership_id = params[:id]
    # action_name = params[:action_name].to_s.downcase
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @membership = Membership.find(membership_id)
    @usermember = Membership.where(group_id: @membership.group_id, user_id: @user.id).first

    respond_with do |format|
      if @usermember.role.eql?('moderator')
        if @membership.update :role => 'moderator'
          format.json { render :json => { :success => true, :data => @membership.as_json(showInGroup:true), :message => 'Member promoted successfully', :code => "6011" }, status: :ok }
        else
          format.json { render :json => { :success => false, :data => @membership.errors, :message => 'Member promotion failed', :code => "6012" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Member promotion is only for group moderator.', :code => "6008" }, status: :unauthorized }
      end
    end
  end

  # GET /memberships/1/demote
  def demote
    membership_id = params[:id]
    # action_name = params[:action_name].to_s.downcase
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @membership = Membership.find(membership_id)
    @usermember = Membership.where(group_id: @membership.group_id, user_id: @user.id).first

    respond_with do |format|
      if @usermember.role.eql?('moderator')
        if @membership.update :role => 'member'
          format.json { render :json => { :success => true, :data => @membership.as_json(showInGroup:true), :message => 'Member demoted successfully', :code => "6013" }, status: :ok }
        else
          format.json { render :json => { :success => false, :data => @membership.errors, :message => 'Member demotion failed', :code => "6014" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Member demotion is only for group moderator.', :code => "6008" }, status: :unauthorized }
      end
    end
  end

  # GET /memberships/1/kick
  def kick
    membership_id = params[:id]
    # action_name = params[:action_name].to_s.downcase
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @membership = Membership.find(membership_id)
    @usermember = Membership.where(group_id: @membership.group_id, user_id: @user.id).first

    respond_with do |format|
      if @usermember.role.eql?('moderator')
        if @membership.destroy
          format.json { render :json => { :success => true, :data => @membership.as_json(showInGroup:true), :message => 'Member kicked successfully', :code => "6015" }, status: :ok }
        else
          format.json { render :json => { :success => false, :data => @membership.errors, :message => 'Member kick failed', :code => "6016" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Kick member is only for group moderator.', :code => "6008" }, status: :unauthorized }
      end
    end
  end

private
  def create_params
    params.require(:membership).permit(:group_id, :uuid)
  end
end
