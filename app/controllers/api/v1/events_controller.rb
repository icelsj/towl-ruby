class Api::V1::EventsController < Api::ApiController
  respond_to :json

  before_action :authenticate, except: [:index]

  def create
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @event = Event.new(event_params)

    respond_with do |format|
      if @user
        @event.user_id = @user.id
        if @event.save
          @attend = Attendant.new(user_id: @user.id, event_id: @event.id, attend: "going")
          @attend.save
          format.json { render :json => { :success => true, :data => @event, :message => 'Your event is created successfully.', :code => "9002" }, status: :created }
        else
          format.json { render json: { :success => false, :data => @event.errors, :message => 'Failed to create a event.', :code => "9003" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid user owner.', :code => "9001" }, status: :unauthorized }
      end
    end
  end

  def show
    # accesskey = request.headers['X-Access-Key']
    # @user = User.where(accesskey: accesskey).first if accesskey
    # userid = @user.id if @user

    @event = Event.find(params[:id])
    respond_with do |format|
      if @event
        format.json { render :json => { :success => true, :data => @event, :message => 'Found event', :code => "9004" }, status: :ok}
      else
        format.json { render :json => { :success => false, :message => 'not found', :code => "9005" }, status: :forbidden }
      end
    end
  end

  def update
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    userid = @user.id if @user

    @event = Event.find(params[:id])
    respond_with do |format|
      if userid == @event.user_id
        if @event.update(event_params)
          format.json { render :json => { :success => true, :data => @event, :message => 'Event is updated successfully', :code => "9006" }, status: :ok}
        else
          format.json { render :json => { :success => false, :data => @event.errors, :message => 'Event is failed to update', :code => "9007" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'unauthorized', :code => "9001" }, status: :unauthorized }
      end
    end
  end

  def index
    time_col = Event.arel_table[:eventtime]
    time_is_today_or_more = time_col.gteq(1.days.ago)

    params[:page] = 1 unless params.has_key?(:page)
    sport_id = params[:sport_id] if params[:sport_id]
    if sport_id
      @event = Event.where(time_is_today_or_more).where(sport_id:sport_id, available:true).order('eventtime')
    else
      @event = Event.where(time_is_today_or_more).where(available:true).order('eventtime')
    end

    @group_entries = @event.page(params[:page])
    @absolute_url = request.host
    if request.host.eql?('localhost') then
      @absolute_url = request.host_with_port
    end
    if sport_id
      @absolute_url = @absolute_url.concat('/api/v1/events/sports/').concat(sport_id.to_s).concat('/page/')
    else
      @absolute_url = @absolute_url.concat('/api/v1/events/page/')
    end
    @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
    @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @group_entries.total_pages

    respond_with do |format|
      format.json { render :json => { :success => true,
                                      :total_items => @event.count,
                                      :total_pages => @group_entries.total_pages,
                                      :prev_url => @prev_url,
                                      :next_url => @next_url,
                                      :data => @group_entries,
                                      :code => "9008",
                                      :message => 'List events successfully.'
                           }
      }
    end
  end

  def going
    time_col = Event.arel_table[:eventtime]
    time_is_today_or_more = time_col.gteq(1.days.ago)

    params[:page] = 1 unless params.has_key?(:page)
    sport_id = params[:sport_id] if params[:sport_id]
    if sport_id
      @event = Event.joins(:attendant).where(time_is_today_or_more).where(sport_id:sport_id, available:true, attendants: { user_id: @user.id, attend: ["going", "maybe"]}).order('eventtime')
    else
      @event = Event.joins(:attendant).where(time_is_today_or_more).where(available:true, attendants: { user_id: @user.id, attend: ["going", "maybe"]}).order('eventtime')
    end

    @group_entries = @event.page(params[:page])
    @absolute_url = request.host
    if request.host.eql?('localhost') then
      @absolute_url = request.host_with_port
    end
    if sport_id
      @absolute_url = @absolute_url.concat('/api/v1/events/going/sports/').concat(sport_id.to_s).concat('/page/')
    else
      @absolute_url = @absolute_url.concat('/api/v1/events/going/page/')
    end
    @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
    @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @group_entries.total_pages

    respond_with do |format|
      format.json { render :json => { :success => true,
                                      :total_items => @event.count,
                                      :total_pages => @group_entries.total_pages,
                                      :prev_url => @prev_url,
                                      :next_url => @next_url,
                                      :data => @group_entries,
                                      :code => "9008",
                                      :message => 'List events successfully.'
                           }
      }
    end
  end
private
  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:name, :description, :sport_id, :privacy, :venue, :eventtime)
  end
end
