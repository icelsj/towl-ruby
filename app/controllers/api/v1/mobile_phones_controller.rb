require 'twilio-ruby'

class Api::V1::MobilePhonesController < Api::ApiController
  respond_to :json

  before_action :authenticate_hash

  def createPin
    mobile_no = params[:mobile_no]

    if (!mobile_no.to_s.start_with? '+')
      mobile_no = '+'.concat mobile_no
    end

    respond_with do |format|
      if (verifyNumber? mobile_no)
        @mobile = MobilePhone.create :mobileno => mobile_no

        strMessage = "TOWL: Your PIN is ".concat @mobile.pin
        begin
          sendSMS @mobile.mobileno, strMessage
          format.json { render :json => { :success => true, :data => "", :message => 'PIN sent to mobile phone number.', :code => "1003" }, status: :ok }
        rescue Exception => err
          format.json { render :json => { :success => false, :code => "1002", :message => err.message }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :code => "1001", :message => 'Invalid mobile phone number.' }, status: :unprocessable_entity }
      end
    end
  end

  def verifyPin
    mobile_no = params[:mobile_no]

    if (!mobile_no.to_s.start_with? '+')
      mobile_no = '+'.concat mobile_no
    end

    @mobile = MobilePhone.where(mobileno: mobile_no).order(expired_at: :desc).first

    respond_with do |format|

      if @mobile
        if @mobile.expired_at < Time.now
          format.json { render :json => { :sucess => false, :expired => true, :message => 'Expired PIN.', :code => "1004" }, status: :unprocessable_entity }
        elsif @mobile.pin.eql? params[:pin]
          @mobile.update verified: true
          @user = User.find_by_mobileno(mobile_no)
          if !@user
            @user = User.create(mobileno: mobile_no, full_name: 'User#'.concat(mobile_no), last_name: 'User', first_name: mobile_no,
                                username: mobile_no)
          else
            @user.after_login
          end

          format.json { render :json => { :success => true, :pin => true, :data => @user, :message => 'Verified successfully.', :code => "1005" }, status: :ok }
        else
          format.json { render :json => { :success => false, :pin => false, :message => 'Invalid PIN.', :code => "1006" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid / not exist mobile phone number.', :code => "1007" }, status: :unprocessable_entity }
      end
    end
  end

  def sendSMS(mobileno, message)
    # put your own credentials here
    account_sid = 'AC1d909084cce29f0fbf8beb97d0c1c139'
    auth_token = 'ad477ed1bf34d18395175371f652e193'

    @twilio_number = '+12028998209'

    # set up a client to talk to the Twilio REST API
    @client = Twilio::REST::Client.new account_sid, auth_token

    @client.messages.create(
        from: @twilio_number,
        to: mobileno,
        body: message
    )
  end

  def verifyNumber?(mobileno)
    begin
      account_sid = 'AC1d909084cce29f0fbf8beb97d0c1c139'
      auth_token = 'ad477ed1bf34d18395175371f652e193'
      lookup_client = Twilio::REST::LookupsClient.new account_sid, auth_token

      response = lookup_client.phone_numbers.get(mobileno)
      response.phone_number
      return true
    rescue
      return false
    end
  end
end
