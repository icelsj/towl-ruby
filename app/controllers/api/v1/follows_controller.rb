class Api::V1::FollowsController < Api::ApiController
  respond_to :json

  before_action :authenticate, only: [:create, :destroy, :followers, :following]

  def create
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    following = params[:username]
    @following = User.find_by_username(following)

    respond_with do |format|
      if @user && @following
        @follows = Follow.where(:follower_id => @user.id, :following_id => @following.id)
        if @follows.count > 0
          format.json { render json: { :success => false, :message => 'You already had followed the user.', :code => "3001" }, status: :unprocessable_entity }
        else
          @follows = Follow.new(:follower_id => @user.id, :following_id => @following.id)
          if @follows.save
            # return data
            format.json { render :json => { :success => true, :data => @follows, :message => 'You have successfully follow.', :code => "3002" }, status: :created }
          else
            format.json { render json: { :success => false, :data => @follows.errors, :message => 'Failed to follow.', :code => "3003" }, status: :unprocessable_entity }
          end
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "3004" }, status: :unauthorized }
      end
    end
  end

  def destroy
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    following = params[:username]
    @following = User.find_by_username(following)

    respond_with do |format|
      if @user && @following
        @follows = Follow.where(:follower_id => @user.id, :following_id => @following.id)
        if @follows.count <= 0
          format.json { render json: { :success => false, :message => 'You have not follow the user.', :code => "3005" }, status: :unprocessable_entity }
        else
          @follows = @follows.first
          if @follows.destroy
            # return data
            format.json { render :json => { :success => true, :data => @follows, :message => 'You have successfully unfollow.', :code => "3006" }, status: :created }
          else
            format.json { render json: { :success => false, :data => @follows.errors, :message => 'Failed to unfollow.', :code => "3007" }, status: :unprocessable_entity }
          end
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "3004" }, status: :unauthorized }
      end
    end
  end

  def followers
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    following = params[:username]
    @following = User.find_by_username(following)

    respond_with do |format|
      if @user && @following
        @follows = Follow.where(:following_id => @following.id)
        # return data
        format.json { render :json => { :success => true, :data => @follows, :message => 'Followers list successfully.', :code => "3008" }, status: :created }
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "3004" }, status: :unprocessable_entity }
      end
    end
  end

  def following
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    following = params[:username]
    @following = User.find_by_username(following)

    respond_with do |format|
      if @user && @following
        @follows = Follow.where(:follower_id => @following.id)
          # return data
          format.json { render :json => { :success => true, :data => @follows, :message => 'Following list successfully.', :code => "3009" }, status: :created }
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "3004" }, status: :unprocessable_entity }
      end
    end
  end
end
