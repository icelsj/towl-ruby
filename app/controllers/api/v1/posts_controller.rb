class Api::V1::PostsController < Api::ApiController
  respond_to :json

  before_action :authenticate, only: [:create, :listgrouppost, :listfeed]

  def create
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @post = Post.new(post_params)
    # @sport = Sport.find(@post.id)
    groupauthorized = true

    if @post.targettype.eql? "group"
      @membership = Membership.where(group_id:@post.targetid, user_id:@user.id)
      if @membership.count <= 0
        groupauthorized = false
      end
    end

    respond_with do |format|
      if @user && groupauthorized
        @post.user_id = @user.id
        if @sport = Sport.find_by_id(params[:post][:sport_id])
          if @post.save
            # return data
            format.json { render :json => { :success => true, :data => @post, :message => 'Your post is posted successfully.', :code => "7001" }, status: :created }
          else
            format.json { render json: { :success => false, :data => @post.errors, :message => 'Failed to post.', :code => "7002" }, status: :unprocessable_entity }
          end
        else
          format.json { render :json => { :success => false, :message => 'Invalid sport.', :code => "7003" }, status: :unprocessable_entity }
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "7004" }, status: :unauthorized }
      end
    end
  end

  def destroy
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    @post = Post.find(params[:id])
    groupauthorized = true

    if @post.targettype.eql? "group"
      @membership = Membership.where(group_id:@post.targetid, user_id:@user.id) if @user
      if !@membership || @membership.count <= 0
        groupauthorized = false
      elsif @membership.first.role.eql? "moderator"
        groupauthorized = true
      else
        groupauthorized = false
      end
    end

    respond_with do |format|
      if !@user || (@post.user_id != @user.id && !groupauthorized)
        format.json { render :json => { :success => false, :message => 'Not post author.', :code => "7004" }, status: :unauthorized }
      elsif @post.update :available => false
        format.json { render :json => { :success => true, :data => "", :message => 'Post is removed successfully.', :code => "7005" }, status: :created }
      else
        format.json { render json: { :success => false, :data => @post.errors, :message => 'Failed to remove post.', :code => "7006" }, status: :unprocessable_entity }
      end
    end
  end

  def listgrouppost
    group_id = params[:group_id]
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    params[:page] = 1 unless params.has_key?(:page)

    respond_with do |format|
      if @user
        @membership = Membership.where(group_id:group_id, user_id:@user.id)
        if @membership.count == 0
          format.json { render :json => { :success => false, :message => 'User is not joined yet.', :code => "7004" }, status: :unauthorized }
        else

          @post = Post.where(targettype:"group", targetid:group_id, available:true).order('created_at desc')
          @post_entries = @post.page(params[:page])
          @absolute_url = request.host
          if request.host.eql?('localhost') then
            @absolute_url = request.host_with_port
          end
          @absolute_url = @absolute_url.concat('/api/v1/groups/').concat(params[:group_id]).concat('/posts/page/')
          @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
          @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @post_entries.total_pages
          format.json { render :json => { :success => true,
                                          :total_items => @post.count,
                                          :total_pages => @post_entries.total_pages,
                                          :prev_url => @prev_url,
                                          :next_url => @next_url,
                                          :data => @post_entries,
                                          :message => 'Group post listing successfully.',
                                          :code => "7007" },
                               status: :ok }
        end
      else
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "7004" }, status: :unauthorized }
      end
    end
  end

  def listuserpost
    username = params[:username]
    params[:page] = 1 unless params.has_key?(:page)

    respond_with do |format|

        @post = Post.where(targettype:"user", targetid:username, available:true).order('created_at desc')
        @post_entries = @post.page(params[:page])
        @absolute_url = request.host
        if request.host.eql?('localhost') then
          @absolute_url = request.host_with_port
        end
        @absolute_url = @absolute_url.concat('/api/v1/users/').concat(params[:username]).concat('/posts/page/')
        @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
        @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @post_entries.total_pages
        format.json { render :json => { :success => true,
                                        :total_items => @post.count,
                                        :total_pages => @post_entries.total_pages,
                                        :prev_url => @prev_url,
                                        :next_url => @next_url,
                                        :data => @post_entries,
                                        :message => 'User post listing successfully.',
                                        :code => "7008" },
                             status: :ok }

      # else
      #   format.json { render :json => { :success => false, :message => 'Invalid user.', status: :unprocessable_entity } }
      # end
    end
  end

  def listfeed
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey).first if accesskey
    params[:page] = 1 unless params.has_key?(:page)

    respond_with do |format|

      @post = Post.where('id IN (SELECT id
      FROM posts WHERE targettype LIKE \'group\' AND targetid IN
      (SELECT CAST(group_id AS CHARACTER VARYING) FROM memberships WHERE user_id = ? AND status LIKE \'joined\'))
      OR id IN (SELECT id
      FROM posts WHERE targettype LIKE \'user\' AND targetid IN
      (SELECT username FROM users WHERE id IN (SELECT following_id FROM follows WHERE follower_id = ?)))
      OR id IN (SELECT id
      FROM posts WHERE targettype LIKE \'user\' AND targetid = (SELECT username FROM users WHERE id = ?))', @user.id, @user.id, @user.id).where(available:true).order("created_at desc")

      @post_entries = @post.page(params[:page])
      @absolute_url = request.host
      if request.host.eql?('localhost') then
        @absolute_url = request.host_with_port
      end
      @absolute_url = @absolute_url.concat('/api/v1/feeds/page/')
      @prev_url = @absolute_url + (params[:page].to_i - 1).to_s unless params[:page].to_i - 1 <= 0
      @next_url = @absolute_url + (params[:page].to_i + 1).to_s unless params[:page].to_i + 1 > @post_entries.total_pages
      format.json { render :json => { :success => true,
                                      :total_items => @post.count,
                                      :total_pages => @post_entries.total_pages,
                                      :prev_url => @prev_url,
                                      :next_url => @next_url,
                                      :data => @post_entries,
                                      :message => 'News feed listing successfully.',
                                      :code => "7008" },
                           status: :ok }

      # else
      #   format.json { render :json => { :success => false, :message => 'Invalid user.', status: :unprocessable_entity } }
      # end
    end
  end

private
  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.require(:post).permit(:message, :posttype, :targettype, :targetid, :sport_id)
  end
end
