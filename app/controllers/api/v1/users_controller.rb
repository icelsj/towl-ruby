

class Api::V1::UsersController < Api::ApiController
  respond_to :json

  before_action :authenticate, except: [:create, :login, :fblogin, :getavatar, :getcover, :show]
  before_action :authenticate_hash, only: [:create, :login, :fblogin]

  # GET /users
  def index
    # @user = User.all
    # respond_with do |format|
    #   format.json { render :json => { :success => 1, :data => @user }, :except => [:accesskey] }
    # end
    respond_with do |format|
      format.json { render :json => { :sucess => false, :message => 'unused.' }, status: :unauthorized }
    end
  end

  # Registration
  # POST /users
  def create
    @user = User.new(new_user_params)
    @user.password = params[:password]
    @user.password_confirmation = params[:password_confirmation]
    respond_to do |format|
      if @user.save
        format.json { render :json => { :success => true, :data => @user, :message => 'Thank you for registration.', :code => "2001" }, :except => [:password_digest] , status: :created }
      else
        format.json { render json: { :success => false, :data => @user.errors, :message => 'Sorry, registration is failed.', :code => "2002" }, status: :unprocessable_entity }
      end
    end
  end

  # Get Profile
  # GET /users/1
  def show
    username = params[:username]
    @user = User.where(username: username).first if username
    respond_with do |format|
      if @user
        format.json { render :json => { :success => true, :data => @user, :message => 'found.', :code => "2003" }, :except => [:password_digest, :accesskey], status: :ok}
      else
        format.json { render :json => { :success => false, :message => 'User is not exist', :code => "2004" }, status: :ok }
      end
    end
  end

  # GET /users/username/login
  def login
    username = params[:username]
    @user = User.find_by_username(username.to_s.downcase) if username

    if @user.authenticate(params[:password])
      @user.after_login

      respond_with do |format|
        if @user
          format.json { render :json => { :success => true, :data => @user, :code => "2005", :message => 'Login successful.' }, :except => [:password_digest], status: :ok}
        else
          format.json { render :json => { :success => false, :message => 'Failed to login.', :code => "2006" }, status: :unprocessable_entity }
        end
      end
    else
      respond_with do |format|
        format.json { render :json => { :success => false, :message => 'Invalid username or password.', :code => "2007" }, status: :unprocessable_entity }
      end
    end
  end

  # POST new username
  def changeusername
    accesskey = request.headers['X-Access-Key']
    username = params[:username]
    new_username = params[:new_username]
    @user = User.where(username: username.downcase, accesskey: accesskey).first if username && accesskey

    respond_to do |format|
      if !@user
        format.json { render json: { :success => false, :message => 'User not found.', :code => "2004" }, status: :unprocessable_entity }
      elsif @user.update(username: new_username.to_s.downcase)
        format.json { render :json => { :success => true, :data => @user, :message => 'Username was successfully updated.', :code => "2013" }, :except => [:password_digest, :accesskey], status: :ok }
      else
        format.json { render json: { :success => false, :data => @user.errors, :message => 'Username was failed update.', :code => "2014" }, status: :unprocessable_entity }
      end
    end
  end

  # GET /users/1/login# def fblogin
  def fblogin
    uid = params[:uid]
    code = params[:code]
    provider = params[:provider]
    email = params[:email]


    # @userprofile = Omniauth::Facebook.authenticate(code)

    respond_with do |format|
      if !(uid && provider && email && code)
        format.json { render :json => { :success => false, :message => 'incomplete data', :code => "2008" }, status: :unprocessable_entity }
      else
        case provider
          when 'facebook'
            @graph = Koala::Facebook::API.new(code)
            profile = @graph.get_object('me?fields=id,name,email')
            if profile && profile['email'] == email && profile['id'] == uid
              if Authorization.find_by_provider_and_uid(provider, uid)
                if @user = User.find_by_email(email)
                  @user.after_login
                  format.json { render :json => { :success => true, :data => @user, :message => 'User login successfully.', :code => "2005" }, :except => [:password_digest], status: :ok }
                else
                  format.json { render :json => { :success => false, :signup => true, :message => 'User facebook not found.', :code => "2009" }, status: :ok }
                end
              else
                @user.after_login
                @auth = Authorization.create :email => email, :provider => provider, :uid => uid
                format.json { render :json => { :success => true, :data => @user, :signup => true, :message => 'User facebook found.', :code => "2010" }, status: :ok }
              end
            else
              format.json { render :json => { :success => false, :message => 'invalid token', :code => "2011" }, status: :unprocessable_entity }
            end
          else
            format.json { render :json => { :success => false, :message => 'unsupported service', :code => "2012" }, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /users/1
  def update
    accesskey = request.headers['X-Access-Key']
    username = params[:username]
    @user = User.where(username: username, accesskey: accesskey).first if username && accesskey

    respond_to do |format|
      if !@user
        format.json { render json: { :success => false, :message => 'User not found.', :code => "2004" }, status: :unprocessable_entity }
      # elsif !check_update_params
      #   format.json { render json: { :success => false, :message => 'User was failed update.' }, status: :unprocessable_entity }
      elsif @user.update(update_user_params)
        format.json { render :json => { :success => true, :data => @user, :message => 'User was successfully updated.', :code => "2013" }, :except => [:password_digest, :accesskey], status: :ok }
      else
        format.json { render json: { :success => false, :data => @user.errors, :message => 'User was failed update.', :code => "2014" }, status: :unprocessable_entity }
      end
    end
  end

  # Avatar Upload
  # POST /users/1/avatar
  def postavatar
    accesskey = request.headers['X-Access-Key']
    username = params[:username]
    @user = User.where(username: username, accesskey: accesskey).first if username && accesskey


    respond_to do |format|
      begin
        params[:image] = parse_image_data(params[:image]) if params[:image]
        @user.avatar = params[:image]
        if @user.save
          format.json { render :json => { :success => true, :data => @user, :message => 'User\'s photo was successfully uploaded.', :code => "2015" }, :except => [:password_digest, :accesskey], status: :ok }
        else
          format.json { render json: { :success => false, :message => 'User\'s photo was failed update.', :code => "2016" }, status: :unprocessable_entity }
        end
      rescue Exception => e
        Rails.logger.error "#{e.message}"
        format.json { render json: { :success => false, :message => e.message, :code => "2017" }, status: :unprocessable_entity }
      end
    end
  end

  # Avatar Link
  # GET /users/1/avatar
  def getavatar
    username = params[:username]
    @user = User.where(username: username).first if username

    if @user.avatar.url(:original).to_s.eql?('/filecovers/original/missing.png')
      respond_to do |format|
        format.json { render json: { :success => false, :message => 'not found.', :code => "2018" }, status: :not_found }
      end
    else
      redirect_to @user.avatar.url(:original)
    end
  end

  # Avatar Upload
  # POST /users/1/avatar
  def postcover
    accesskey = request.headers['X-Access-Key']
    username = params[:username]
    @user = User.where(username: username, accesskey: accesskey).first if username && accesskey


    respond_to do |format|
      begin
        params[:image] = parse_image_data(params[:image]) if params[:image]
        @user.fileCover = params[:image]
        if @user.save
          format.json { render :json => { :success => true, :data => @user, :message => 'User\'s photo was successfully uploaded.', :code => "2015" }, :except => [:password_digest, :accesskey], status: :ok }
        else
          format.json { render json: { :success => false, :message => 'User photo was failed update.', :code => "2016" }, status: :unprocessable_entity }
        end
      rescue Exception => e
        Rails.logger.error "#{e.message}"
        format.json { render json: { :success => false, :message => e.message, :code => "2017" }, status: :unprocessable_entity }
      end
    end
  end

  # Avatar Link
  # GET /users/1/avatar
  def getcover
    username = params[:username]
    @user = User.where(username: username).first if username

    if @user.fileCover.url(:original).to_s.eql?('/filecovers/original/missing.png')
      respond_to do |format|
        format.json { render json: { :success => false, :message => 'not found.', :code => "2018" }, status: :not_found }
      end
    else
      redirect_to @user.fileCover.url(:original)
    end
  end

private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def new_user_params
    params.require(:user).permit(:mobileno, :first_name, :last_name, :full_name, :email, :username, :password, :password_confirmation)
  end

  def update_user_params
    params.require(:user).permit(:first_name, :last_name, :full_name)
  end

  def check_update_params
    return params.has_key?(:first_name) || params.has_key?(:last_name) || params.has_key?(:full_name)
  end

  def parse_image_data(image_data)
    @tempfile = Tempfile.new('item_image')
    @tempfile.binmode
    @tempfile.write Base64.decode64(image_data[:content])
    @tempfile.rewind

    uploaded_file = ActionDispatch::Http::UploadedFile.new(
        tempfile: @tempfile,
        filename: image_data[:filename]
    )

    uploaded_file.content_type = image_data[:content_type]
    uploaded_file
  end

  def clean_tempfile
    if @tempfile
      @tempfile.close
      @tempfile.unlink
    end
  end
end
