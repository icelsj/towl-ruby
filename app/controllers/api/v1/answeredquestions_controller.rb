class Api::V1::AnsweredquestionsController < Api::ApiController
  respond_to :json

  # before_action :authenticate_hash

  def create
    @answeredquestion = Answeredquestion.new(new_answer_params)
    @answeredquestion.othersport = params[:othersport] if params[:othersport]
    respond_to do |format|
      if @answeredquestion.save
        format.json { render :json => { :success => true, :data => @answeredquestion, :message => 'Thank you for answer.' } , status: :created }
      else
        format.json { render json: { :success => false, :data => @answeredquestion.errors, :message => 'Sorry, answer submission failed.' }, status: :unprocessable_entity }
      end
    end
  end

  def update
    @answeredquestion = Answeredquestion.find_by_questionkey(params[:questionkey])
    @answeredquestion.update(email:params[:email], addedemail_at:Time.current)
    respond_to do |format|
      if @answeredquestion.save
        format.json { render :json => { :success => true, :data => @answeredquestion, :message => 'Thank you subscribe.' } , status: :created }
      else
        format.json { render json: { :success => false, :data => @answeredquestion.errors, :message => 'Sorry, subscription failed.' }, status: :unprocessable_entity }
      end
    end
  end
private
  # Never trust parameters from the scary internet, only allow the white list through.
  def new_answer_params
    params.require(:answeredquestion).permit(:name, :birthday, :gender, :sport, :reason, :frequency)
  end
end
