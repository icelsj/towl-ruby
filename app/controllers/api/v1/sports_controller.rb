class Api::V1::SportsController < Api::ApiController
  respond_to :json

  before_action :authenticate, :only => [:likesports, :unlikesports]

  # GET /sports
  def index
    @sports = Sport.all
    respond_with do |format|
      format.json { render :json => { :success => true, :data => @sports, :message => 'Sports list successfully.', :code => "4001" } }
    end
  end

  def usersports
    username = params[:username]
    @user = User.where(username: username).first if username

    unless @user
      respond_with do |format|
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "4002" }, status: :unauthorized }
      end
    else
      @sports = Sportspref.where(user_id: @user.id)
      respond_with do |format|
        format.json { render :json => { :success => true, :data => @sports, :message => 'User\'s interest list successfully.', :code => "4003" }, status: :ok }
      end
    end
  end

  def likesports
    username = params[:username]
    sports = params[:sports]
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey, username: username).first if accesskey && username

    unless @user
      respond_with do |format|
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "4002" }, status: :unauthorized }
      end
    else
      sports.each do |sport|
        if Sportspref.where(user_id: @user.id, sport_id: sport).count <= 0
          @sportspref = Sportspref.create :user_id => @user.id, :sport_id => sport
        end
      end

      usersports
    end
  end

  def unlikesports
    username = params[:username]
    sports = params[:sports]
    accesskey = request.headers['X-Access-Key']
    @user = User.where(accesskey: accesskey, username: username).first if accesskey && username

    unless @user
      respond_with do |format|
        format.json { render :json => { :success => false, :message => 'Invalid user.', :code => "4002" }, status: :unauthorized }
      end
    else
      sports.each do |sport|
        if @sportspref = Sportspref.where(user_id: @user.id, sport_id: sport).first
          @sportspref.destroy
        end
      end

      usersports
    end
  end

end
