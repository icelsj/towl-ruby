class SessionsController < ApplicationController
  def new
    @cmsuser = Cmsuser.new
  end

  def create
    @cmsuser = Cmsuser.find_by_username(params[:session][:username].to_s.downcase)

    respond_to do |format|
      if @cmsuser && @cmsuser.authenticate(params[:session][:password])
        session[:admin_id] = @cmsuser.uuid
        format.html { redirect_to root_path, notice: 'Login successfully' }
        format.json { render root_path, status: :created, location: @cmsuser }
      else
        @cmsuser = Cmsuser.new
        @cmsuser.errors.add(:username, 'Invalid username and password.')
        format.html { render :new }
        format.json { render json: @cmsuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # def createprovider
  #   auth_hash = request.env['omniauth.auth']
  #
  #   if session[:user_id]
  #     # Means our user is signed in. Add the authorization to the user
  #     User.find(session[:user_id]).add_provider(auth_hash)
  #
  #     render :text => "You can now login using #{auth_hash["provider"].capitalize} too!"
  #   else
  #     # Log him in or sign him up
  #     auth = Authorization.find_or_create(auth_hash)
  #
  #     # Create the session
  #     session[:user_id] = auth.user.id
  #
  #     render :text => "Welcome #{auth.user.name}!"
  #   end
  # end
  #
  # def failure
  #   render :text => "Sorry, but you didn't allow access to our app!"
  # end

  def destroy
    session[:admin_id] = nil
    redirect_to '/'
  end

private
  def add_provider(auth_hash)
    # Check if the provider already exists, so we don't add it twice
    unless Authorizations.find_by_provider_and_uid(auth_hash["provider"], auth_hash["uid"])
      Authorization.create :user => self, :provider => auth_hash["provider"], :uid => auth_hash["uid"]
    end
  end
end
