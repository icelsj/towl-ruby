class PagesController < ApplicationController
  layout "homelayout"

  def index
    @current ||= Cmsuser.where(uuid: session[:admin_id]).first if session[:admin_id]
  end
end
