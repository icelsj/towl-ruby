class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user

  validates :user_id, presence: true
  validates :post_id, presence: true
  validates :message, presence: true

  before_create do |doc|
    doc.available = true
  end

  # To_Json
  def user
    @user = User.find(user_id).as_json(:only => [:username, :first_name, :last_name, :full_name, :id, :uuid])
  end

  def post
    @post = Post.find(post_id).as_json
  end

  def as_json(options={})
    super(:except => [:user_id],
          :methods => [:user]
    )
  end
end
