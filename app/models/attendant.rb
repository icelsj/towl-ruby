class Attendant < ActiveRecord::Base
  belongs_to :user
  belongs_to :event

  validates :attend, presence: true
end
