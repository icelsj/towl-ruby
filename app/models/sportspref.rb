class Sportspref < ActiveRecord::Base
  belongs_to :user
  belongs_to :sport

  #to_json
  def sport
    @sport = Sport.find(self.sport_id)
  end

  def as_json(options={})
    super(:except => [:user_id, :sport_id], :methods => [:sport])
  end
end
