class Post < ActiveRecord::Base
  belongs_to :user
  belongs_to :sport

  validates :user_id, presence: true
  validates :sport_id, presence: true
  validates :message, presence: true
  validates :posttype, presence: true
  validates :targettype, presence: true
  validates :targetid, presence: true

  before_create do |doc|
    doc.available = true
  end

  # To_Json
  def user
    @user = User.find(user_id).as_json(:only => [:username, :first_name, :last_name, :full_name, :id, :uuid])
  end

  def sport
    if @sport = Sport.find(sport_id).as_json
      @sport
    end
  end

  def commentscount
    @count = Comment.where(post_id:id, available:true).count
  end

  def comments
    @comment = Comment.where(post_id:id, available:true).order('created_at desc').limit(5)
  end

  def as_json(options={})
    super(:except => [:user_id, :group_id, :sport_id],
          :methods => [:user, :sport, :commentscount, :comments]
    )
  end
end
