class Answeredquestion < ActiveRecord::Base
  # Generator for ids
  def generate_accesskey
    loop do
      token = SecureRandom.base64.tr('+/=', 'PkS')
      break token unless Answeredquestion.where(questionkey: token).any?
    end
  end

  # Action for pre or post actions
  before_create do |doc|
    doc.questionkey = doc.generate_accesskey
    doc.answered_at = Time.current
    doc.sport = doc.sport.to_s.downcase
    doc.othersport = doc.othersport.to_s.downcase
    doc.reason = doc.reason.to_s.downcase
    doc.frequency = doc.frequency.to_s.downcase
  end
end
