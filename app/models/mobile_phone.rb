class MobilePhone < ActiveRecord::Base
  validates :mobileno, presence: true
  # validates :pin, presence: true
  # validates :expired_at, presence: true
  # validates :verified, :inclusion => {:in => [true, false]}

  # Generator for ids
  def generate_pin(length)
    rand.to_s[2..(length + 1)]
  end

  before_create do |doc|
    doc.pin = doc.generate_pin 6
    # doc.expired_at = Time.now + 35 * 60
    doc.expired_at = Time.now + 35 * 60 * 60 * 24 * 365
    doc.verified = false
    true
  end

end
