class User < ActiveRecord::Base
  # main
  has_many :authorizations
  has_many :memberships
  has_many :groups
  has_secure_password :validations => false

  # REGEX list
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  # attr_accessible :fileBg, :fileCover, :fileIcon
  has_attached_file :avatar, styles: { large: "960", medium: "640", small: "320", thumb: "100", square: "200x200!" }
  has_attached_file :fileCover, styles: { large: "960", medium: "640", small: "320", thumb: "100" }

  # Validate the attached image is image/jpg, image/png, etc
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :fileCover, :content_type => /\Aimage\/.*\Z/

  # Validation
  validates :mobileno, presence: true, length: { maximum: 20 }, uniqueness: { case_sensitive: false }
  validates :first_name, length: { maximum: 50 }
  validates :last_name, length: { maximum: 50 }
  validates :full_name, presence: true, length: { maximum: 100 }
  validates :username, presence: true, length: { minimum: 3, maximum: 50 }, uniqueness: { case_sensitive: false }
  # validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  # validates_uniqueness_of :facebook_id, :case_sensitive => false, :allow_blank => true, :allow_nil => true

  # Generator for ids
  def generate_accesskey
    loop do
      token = SecureRandom.base64.tr('+/=', 'PkS')
      break token unless User.where(accesskey: token).any?
    end
  end

  def generate_uuid
    loop do
      token = SecureRandom.uuid
      break token unless User.where(uuid: token).any?
    end
  end

  # Action to use by class
  def after_login
    self.update_attributes(:accesskey => self.generate_accesskey)
    self.update_attributes(:last_login => Time.now)
  end

  # Action for pre or post actions
  before_create do |doc|
    doc.accesskey = doc.generate_accesskey
    doc.uuid = doc.generate_uuid
    doc.status = 'active'
    doc.last_login = Time.now
  end

  #To_Json
  def avatar_url
    avatar.url(:original) unless avatar.url(:original).to_s.eql?('/avatars/original/missing.png')
  end

  def cover_url
    fileCover.url(:original) unless fileCover.url(:original).to_s.eql?('/filecovers/original/missing.png')
  end

  def as_json(options={})

    super(options).merge(:avatar => avatar_url, :cover => cover_url)
  end
end