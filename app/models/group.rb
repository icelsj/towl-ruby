class Group < ActiveRecord::Base
  belongs_to :sport
  belongs_to :user
  has_many :memberships

  # Validation
  validates :name, presence: true, length: { minimum: 2, maximum: 100 }
  validates :sport_id, presence: true
  validates :user_id, presence: true
  validates :group_type, presence: true
  validates :privacy, presence: true

  # To_Json
  def user
    @user = User.find(user_id).as_json(:only => [:username, :username, :first_name, :last_name, :full_name, :id, :uuid])
  end

  def sport
    @sport = Sport.find(sport_id).as_json
  end

  def joined(user_id = nil)
    if user_id
      @joined = !Membership.where(user_id: user_id, group_id: self.id).blank?
    else
      @joined = false
    end
  end

  def membershipstatus(user_id = nil)
    if user_id
      membership = Membership.where(user_id: user_id, group_id: self.id).first
      @membershipstatus = membership.status if membership
    else
      @membershipstatus = nil;
    end
  end

  def member
    @member = Membership.where(group_id: self.id, status: 'joined').as_json(showInGroup:true)
  end

  def as_json(options={})
    if options[:user_id].blank?
      super(:except => [:user_id, :sport_id],
            :methods => [:user, :sport, :member]
      )
    else
      userid = options[:user_id] unless options[:user_id].blank?
      super(:except => [:user_id, :sport_id],
            :methods => [:user, :sport, :member]
      ).merge({ joined: joined(userid), membershipstatus: membershipstatus(userid) })
    end
  end
end
