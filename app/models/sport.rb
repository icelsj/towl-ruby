class Sport < ActiveRecord::Base
  # main
  has_many :groups

  # attr_accessible :fileBg, :fileCover, :fileIcon
  has_attached_file :fileBg, styles: { large: "960", medium: "640", small: "320", thumb: "100" }
  has_attached_file :fileCover, styles: { large: "960", medium: "640", small: "320", thumb: "100" }
  has_attached_file :fileIcon, styles: { large: "960", medium: "640", small: "320", thumb: "100", square: "200x200!" }

  # Validate the attached image is image/jpg, image/png, etc
  validates_attachment_content_type :fileBg, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :fileCover, :content_type => /\Aimage\/.*\Z/
  validates_attachment_content_type :fileIcon, :content_type => /\Aimage\/.*\Z/

  validates_presence_of :name
  validates_presence_of :detail

  # To_Json
  def imgBg
    fileBg.url(:original)
  end

  def imgCover
    fileCover.url(:original)
  end

  def imgIcon
    fileIcon.url(:original)
  end

  def as_json(options={})
    super(:only => [:id, :name, :detail, :sort, :updated_at],
          :methods => [:imgBg, :imgCover, :imgIcon]
    )
  end
end
