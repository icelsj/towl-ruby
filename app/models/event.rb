class Event < ActiveRecord::Base
  belongs_to :sport
  belongs_to :user

  has_many :attendant

  validates :user_id, presence: true
  validates :sport_id, presence: true
  validates :venue, presence: true
  validates :eventtime, presence: true
  validates :name, presence: true, length: { minimum: 3 }
  validates :privacy, presence: true

  before_create do |doc|
    doc.available = true
  end
end
