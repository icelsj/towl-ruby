class Cmsuser < ActiveRecord::Base
  has_secure_password
  # REGEX list
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  VALID_STRONG_PASSWORD = /((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,50})/

  #validation
  validates :full_name, presence: true, length: { minimum: 3, maximum: 50 }
  validates :username, presence: true, length: { maximum: 255 }, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 8, maximum: 50 }, format: { with: VALID_STRONG_PASSWORD }

  # Generator for ids
  def generate_uuid
    loop do
      token = SecureRandom.uuid
      break token unless User.where(uuid: token).any?
    end
  end

  # Action for pre or post actions
  before_save do |doc|
    doc.uuid = doc.generate_uuid
    doc.status = 1
    doc.role = 1
  end
end
