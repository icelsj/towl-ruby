class Follow < ActiveRecord::Base

  # To_Json
  def following
    @user = User.find(following_id).as_json(:only => [:username, :first_name, :last_name, :full_name, :id, :uuid])
  end

  def follower
    @user = User.find(follower_id).as_json(:only => [:username, :first_name, :last_name, :full_name, :id, :uuid])
  end

  def as_json(options={})
    super(:except => [:follower_id, :following_id],
          :methods => [:following, :follower]
    )
  end
end
