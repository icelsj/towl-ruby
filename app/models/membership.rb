class Membership < ActiveRecord::Base
  belongs_to :group
  belongs_to :user

  # Validation
  validates :group_id, presence: true
  validates :user_id, presence: true
  validates :role, presence: true
  validates :status, presence: true


  # To_Json
  def user
    @user = User.find(user_id).as_json(:only => [:username, :first_name, :last_name, :full_name, :id, :uuid])
  end

  def group
    @group = Group.find(group_id).as_json
  end

  def as_json(options={})
    if options[:showInGroup].blank?
      super(:except => [:user_id, :group_id],
            :methods => [:user, :group]
      )
    else
      super(:except => [:user_id, :group_id],
            :methods => [:user]
      )
    end
  end
end
