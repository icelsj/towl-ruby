# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ -> (
  $('#txtBirthday').datepicker(
    changeYear:true,
    yearRange:"1900:2050",
    changeMonth:true,
    maxDate: '0'
  )

  $('.btnNext').click (event) -> (
    event.preventDefault()
    rel = $(this).attr('rel')
    nextrel = parseInt(rel) + 1

#    if parseInt(rel) == 1 && $('#txtName').val().length != 0 && $('#txtBirthday').val().length != 0
#      $('#lblName').html($('#txtName').val())
#    else if parseInt(rel) == 1
#      alert('Please tell us your name and birthday.')
#      return
    if rel == '5'
      $('#lblName').html($('#txtName').val())
      gender = 'male'
      if ($('#cbFemale').hasClass('selected'))
        gender = 'female'

      $.ajax '/api/v1/answeredquestions',
        type: 'POST'
        dataType: 'json'
        contentType: 'application/json'
        data: JSON.stringify({
          name: $('#txtName').val(),
          birthday: $('#txtBirthday').val(),
          gender: gender,
          sport: $('#cbSports').val(),
          othersport: $('#txtOtherSports').val(),
          reason: $('#cbPurpose').val(),
          frequency: $('#cbOften').val()
        })
        error: (jqXHR, textStatus, errorThrown) ->
          alert textStatus
        success: (data, textStatus, jqXHR) ->
          console.log data
          $('#qkey').val(data.data.questionkey)

    $('.question-main-container[rel='+rel+']').hide()
    $('.question-main-container[rel='+nextrel+']').show()
  )

  $('.btnBack').click (event) -> (
    event.preventDefault()
    rel = $(this).attr('rel')
    nextrel = parseInt(rel) - 1

    $('.question-main-container[rel='+rel+']').hide()
    $('.question-main-container[rel='+nextrel+']').show()
  )

  $('#btnSubmit').click (event) -> (
    event.preventDefault()
    $.ajax '/api/v1/answeredquestions/'+$('#qkey').val(),
      type: 'PATCH'
      dataType: 'json'
      contentType: 'application/json'
      data: JSON.stringify({
        email: $('#txtEmail').val()
      })
      error: (jqXHR, textStatus, errorThrown) ->
        alert textStatus
      success: (data, textStatus, jqXHR) ->
        console.log data
        $('#txtEmail').hide()
        $('#btnSubmit').hide()
  )

  $('.cbGender').click (event) -> (
    event.preventDefault()
    $('.cbGender').removeClass('selected')
    $(this).addClass('selected')
  )



#  $('#cbSports').selectmenu()
)