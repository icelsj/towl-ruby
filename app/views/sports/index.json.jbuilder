json.array!(@sports) do |sport|
  json.extract! sport, :id, :name, :detail, :imgBg, :imgCover, :imgIcon, :sort
  json.url sport_url(sport, format: :json)
end
