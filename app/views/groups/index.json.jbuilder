json.array!(@groups) do |group|
  json.extract! group, :id, :name, :detail, :sport_id, :owner_id, :type, :privacy
  json.url group_url(group, format: :json)
end
