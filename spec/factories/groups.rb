FactoryGirl.define do
  factory :group do
    user
    sport

    name "Awesome Group"
    detail "something that make you awesome"
    group_type "group"
    privacy "public"
  end
end
