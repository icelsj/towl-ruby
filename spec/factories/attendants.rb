FactoryGirl.define do
  factory :attendant do
    user
    event

    attend "going"
  end
end
