FactoryGirl.define do
  factory :event do
    user
    sport

    name "Awesome Event"
    description "something that make you awesome"
    venue "random"
    eventtime { Time.now }
    privacy "public"
  end
end
