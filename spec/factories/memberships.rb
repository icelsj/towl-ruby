FactoryGirl.define do
  factory :membership do
    group
    user
    role "member"
    status "joined"
  end
end