require 'ffaker'

FactoryGirl.define do
  factory :post do
    user
    sport

    message { FFaker::LoremFR.paragraph }
    posttype "feed"

  end

  trait :grouppost do
    targettype "group"
    targetid 1
  end

  trait :userpost do
    targettype "user"
    targetid { FFaker::Internet.user_name }
  end

end