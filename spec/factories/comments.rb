require 'ffaker'

FactoryGirl.define do
  factory :comment do
    user
    association :post, :grouppost

    message { FFaker::LoremFR.paragraph }

  end
end