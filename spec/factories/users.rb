require 'ffaker'

FactoryGirl.define do
  factory :user do
    # mobileno "+60102729704"
    mobileno { FFaker::PhoneNumber.short_phone_number }
    first_name "Joel"
    last_name "Lee"
    full_name "Joel Lee"
    email { FFaker::Internet.email }
    password "Test1234"
    password_confirmation "Test1234"
    # username { FFaker::InternetSE.user_name }
    username { FFaker::PhoneNumber.short_phone_number }
    accesskey "1f39f9a1392429e163a9de7e5e26ddcf"
  end
end
