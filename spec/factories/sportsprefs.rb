FactoryGirl.define do
  factory :sportspref do
    association :user, factory: :user
    association :sport, factory: :sport
  end
end