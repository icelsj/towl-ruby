require 'spec_helper'

describe Api::V1::UsersController do

  # describe "GET #index" do
  #   before do
  #
  #     request.headers['Accept'] = "application/json"
  #     get :index
  #   end
  #
  #   it { should respond_with 401 }
  # end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        @user_attributes = FactoryGirl.attributes_for :user

        request.headers['Accept'] = "application/json"
        request.headers['X-Check-Key'] = "12345"
        request.headers['X-Sum-Key'] = "1f39f9a1392429e163a9de7e5e26ddcf"
        post :create, { user: @user_attributes }, format: :json
      end

      it { should respond_with 201 }
    end

    context "when is not authorized" do
      before(:each) do
        @user_attributes = FactoryGirl.attributes_for :user
        post :create, { format: 'json', user: @invalid_users_attributes }, format: :json
      end

      it { should respond_with 401 }
    end

    context "when is not created" do
      before(:each) do
        @invalid_users_attributes = { mobileno: " ",
                                       full_name: " " }

        request.headers['Accept'] = "application/json"
        request.headers['X-Check-Key'] = "12345"
        request.headers['X-Sum-Key'] = "1f39f9a1392429e163a9de7e5e26ddcf"
        post :create, { user: @invalid_users_attributes }, format: :json
      end

      it { should respond_with 422 }
    end
  end

  describe "GET #show" do
    context "when is successfully shown" do
      before(:each) do
        @user = FactoryGirl.create :user
        request.headers['Accept'] = "application/json"
        get :show, username: @user.username
      end

      it "returns the not found message" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:data][:email]).to eql @user.email
      end

      it { should respond_with 200 }
    end

    context "when is not successfully shown" do
        before(:each) do
          @user_attributes = FactoryGirl.build(:user)
          # @user = FactoryGirl.create :user
          request.headers['Accept'] = "application/json"
          get :show, username: @user_attributes.username
        end

        it "returns the information about a reporter on a hash" do
          user_response = JSON.parse(response.body, symbolize_names: true)
          expect(user_response[:success]).to eql false
        end

      it { should respond_with 200 }
    end
  end

  describe "PATCH/PUT #update" do
    context "when is successfully updated" do
      before(:each) do
        @user = FactoryGirl.create :user

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        put :update, { format: 'json', username: @user.username, user: { full_name: "Joel Lee SJ" } }, format: :json
      end

      it "returns the information about a reporter on a hash" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:success]).to eql true
      end

      it { should respond_with 200 }
    end

    context "when is not authorized" do
      before(:each) do
        @user = FactoryGirl.create :user
        put :update, { format: 'json', username: @user.username, user: { full_name: "Joel Lee SJ" } }, format: :json
      end

      it { should respond_with 401 }
    end

    describe "POST #changeusername" do
      context "when is successfully updated" do
        before(:each) do
          @user = FactoryGirl.create :user

          request.headers['Accept'] = "application/json"
          request.headers['X-Access-Key'] = @user.accesskey
          post :changeusername, { username: @user.username, new_username: "iiiiiiiii" }, format: :json
        end

        it "returns the information about a reporter on a hash" do
          user_response = JSON.parse(response.body, symbolize_names: true)
          expect(user_response[:success]).to eql true
        end

        it { should respond_with 200 }
      end

      context "when is not authorized" do
        before(:each) do
          @user = FactoryGirl.create :user
          request.headers['Accept'] = "application/json"
          post :changeusername, { username: @user.username, new_username: "iiiiiiiii" }
        end

        it { should respond_with 401 }
      end
    end

    context "when is failed to update" do
      before(:each) do
        @user = FactoryGirl.create :user

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        put :update, { format: 'json', username: @user.username, user: { full_name: " " } }, format: :json
      end

      it { should respond_with 422 }
    end
  end
end
