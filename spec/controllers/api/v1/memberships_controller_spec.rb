require 'spec_helper'

describe Api::V1::MembershipsController do
  describe "POST #create" do
    context "join group successfully" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = FactoryGirl. create :user
        @user = User.find(@user.id)

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :create, { username: @user.username, group_id: @group.id }
      end
      it "ensure joined successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end
  end

  describe "DELETE #destroy" do
    context "join group successfully" do
      before(:each) do
        @membership = FactoryGirl.create :membership
        @group = JSON.parse(@membership.group.to_json, symbolize_names: true)
        @group = Group.find(@group[:id])
        @user = JSON.parse(@membership.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        delete :destroy, { username: @user.username, group_id: @group.id }
      end
      it "ensure joined successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end
  end

  describe "GET #getrequest" do
    context "list requests successfully" do
      before(:each) do
        # @membership = FactoryGirl.create :membership
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
        @membership.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :getrequest, { id: @group.id }
      end

      it "ensure joined successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end

      it { should respond_with 200 }
    end

    context "unauthorized list" do
      before(:each) do
        # @membership = FactoryGirl.create :membership
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
        @membership.save

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey
        get :getrequest, { id: @group.id }
      end

      it { should respond_with 401 }
    end
  end

  describe "POST #approverequest" do
    context "update requests successfully" do
      before(:each) do
        # @membership = FactoryGirl.create :membership
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
        @membership.save

        @newuser = FactoryGirl.create :user

        @newmembership = Membership.new(group_id: @group.id, user_id: @newuser.id, role:'member', status:'pending')
        @newmembership.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :approverequest, { id: @group.id, action_name: 'approve', request_id: @newmembership.id }
      end

      it "ensure joined successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end

      it { should respond_with 200 }
    end

    context "unauthorized list" do
      before(:each) do
        # @membership = FactoryGirl.create :membership
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
        @membership.save

        @newuser = FactoryGirl.create :user

        @newmembership = Membership.new(group_id: @group.id, user_id: @newuser.id, role:'member', status:'pending')
        @newmembership.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @newuser.accesskey
        post :approverequest, { id: @group.id, action_name: 'approve', request_id: @newmembership.id }
      end

      it { should respond_with 401 }
    end
  end

  describe "POST #promote" do
    context "promote successfully" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
        @membership.save

        @newuser = FactoryGirl.create :user

        @newmembership = Membership.new(group_id: @group.id, user_id: @newuser.id, role:'member', status:'joined')
        @newmembership.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :promote, { id: @newmembership.id }
      end
      it "ensure promote successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:data][:role]).to eql "moderator"
      end
      it { should respond_with 200 }
    end
  end

  describe "POST #demote" do
    context "demote successfully" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
        @membership.save

        @newuser = FactoryGirl.create :user

        @newmembership = Membership.new(group_id: @group.id, user_id: @newuser.id, role:'moderator', status:'joined')
        @newmembership.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :demote, { id: @newmembership.id }
      end
      it "ensure demote successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:data][:role]).to eql "member"
      end
      it { should respond_with 200 }
    end
  end

  describe "POST #kick" do
    context "kick successfully" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        @membership = Membership.new(group_id: @group.id, user_id: @user.id, role:'moderator', status:'joined')
        @membership.save

        @newuser = FactoryGirl.create :user

        @newmembership = Membership.new(group_id: @group.id, user_id: @newuser.id, role:'member', status:'joined')
        @newmembership.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :kick, { id: @newmembership.id }
      end
      it "ensure kick successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(Membership.where(group_id: @group.id, user_id: @newuser.id).count).to eql 0
      end
      it { should respond_with 200 }
    end
  end
end
