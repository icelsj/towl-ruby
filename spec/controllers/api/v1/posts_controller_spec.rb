require 'spec_helper'

describe Api::V1::PostsController do
  describe "POST #create" do
    context "post successfully" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')

        @post[:targetid] = @group.id

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        post :create, { post: @post.merge({ sport_id: @sport.id, targetid: @group.id }) }
      end
      it "ensure post successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "not in group" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        # @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')

        @post[:targetid] = @group.id

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        post :create, { post: @post.merge({ sport_id: @sport.id, targetid: @group.id }) }
      end

      it { should respond_with 401 }
    end

    context "invalid sport" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')

        @post[:targetid] = @group.id

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        post :create, { post: @post.merge({ sport_id: 0 }) }
      end
      it "ensure sport invalid" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql false
      end
      it { should respond_with 422 }
    end

    context "unauthorized" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey

        post :create, { post: @post.merge({ sport_id: @sport.id }) }
      end

      it { should respond_with 401 }
    end
  end

  describe "GET #listgrouppost" do
    context "list successfully" do
      before(:each) do
        @post = FactoryGirl.create :post, :grouppost
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])

        @post.targetid = @group.id
        @post.save

        @membership = Membership.create(group: @group, user: @user, role: 'member', status: 'joined')

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :listgrouppost, { group_id: @group.id }
      end

      it "ensure valid" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:id]). to eql @post.id
      end

      it { should respond_with 200}
    end

    context "list failed" do
      before(:each) do
        @post = FactoryGirl.create :post, :grouppost
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])

        @post.targetid = @group.id
        @post.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :listgrouppost, { group_id: @group.id }
      end

      it "ensure invalid" do
        # raise response.body
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql false
      end

      it { should respond_with 401 }
    end
  end

  describe "GET #listuserpost" do
    context "list successfully" do
      before(:each) do
        @post = FactoryGirl.create :post, :userpost
        @user = JSON.parse(@post.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])

        @post.targetid = @user.username
        @post.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :listuserpost, { username: @user.username }
      end

      it "ensure valid" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:id]). to eql @post.id
      end

      it { should respond_with 200}
    end
  end

  describe "DELETE #destroy" do
    context "hide post successfully" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')
        @post[:targetid] = @group.id
        @post[:sport_id] = @sport.id
        @post[:user_id] = @user.id
        @post1 = Post.create(@post)

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        delete :destroy, { :id => @post1.id }
      end
      it "ensure hide post successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "hide post successfully - Admin" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')
        @post[:targetid] = @group.id
        @post[:sport_id] = @sport.id
        @post[:user_id] = @user.id
        @post1 = Post.create(@post)

        @user2 = FactoryGirl.create :user
        @user2 = User.find(@user2[:id])
        @membership2 = Membership.create(:user_id => @user2.id, :group_id => @group.id, :role => 'moderator', :status => 'joined')

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user2.accesskey

        delete :destroy, { :id => @post1.id }
      end
      it "ensure hide post successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')
        @post[:targetid] = @group.id
        @post[:sport_id] = @sport.id
        @post[:user_id] = @user.id
        @post1 = Post.create(@post)

        request.headers['Accept'] = "application/json"

        delete :destroy, { :id => @post1.id }
      end

      it { should respond_with 401 }
    end
  end

  describe "GET #listfeed" do
    context "list successfully" do
      before(:each) do
        @post = FactoryGirl.create :post, :userpost
        @user = JSON.parse(@post.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])

        @post.targetid = @user.username
        @post.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :listfeed
      end

      it "ensure valid" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:id]). to eql @post.id
      end

      it { should respond_with 200}
    end
  end
end
