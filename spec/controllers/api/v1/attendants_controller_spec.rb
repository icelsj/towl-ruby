require 'spec_helper'

describe Api::V1::AttendantsController  do
  describe "POST #invitation_response" do
    context "when is successfully created" do
      before(:each) do
        @event = FactoryGirl.build :event
        @user = FactoryGirl.create :user
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        @event.user_id = @user.id
        @event.sport_id = @sport.id
        @event.save

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :invitation_response, { id: @event.id, action_name:"going" }
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][:attend]).to eql "going"
      end

      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @event = FactoryGirl.build :event
        @user = FactoryGirl.create :user
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        @event.user_id = @user.id
        @event.sport_id = @sport.id
        @event.save

        request.headers['Accept'] = "application/json"

        post :invitation_response, { id: @event.id, action_name:"going" }
      end

      it { should respond_with 401 }
    end
  end
end
