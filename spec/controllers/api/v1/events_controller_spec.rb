require 'spec_helper'

describe Api::V1::EventsController do
  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        @event = FactoryGirl.attributes_for :event
        @user = FactoryGirl.create :user
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :create, { event: @event.merge({ sport_id: @sport[:id] }) }
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][:name]).to eql @event[:name]
      end

      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @event = FactoryGirl.attributes_for :event
        @user = FactoryGirl.create :user
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey
        post :create, { event: @event.merge({ sport_id: @sport[:id] }) }
      end

      it { should respond_with 401 }
    end
  end

  describe "PATCH/PUT #update" do
    context "when is successfully show" do
      before(:each) do
        @event = FactoryGirl.build :event
        @user = JSON.parse(@event.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        @event.sport_id = @sport.id
        @event.user_id = @user.id
        @event.save

        @attendant = Attendant.create user_id: @user.id, event_id: @event.id, attend: "going"

        @event.name = "new name"

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        patch :update, { id: @event.id, event: { name: "new name" } }
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][:name]).to eql "new name"
      end

      it { should respond_with 200 }
    end
  end

  describe "GET #show" do
    context "when is successfully show" do
      before(:each) do
        @event = FactoryGirl.create :event
        @user = JSON.parse(@event.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :show, { id: @event.id }
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][:name]).to eql @event[:name]
      end

      it { should respond_with 200 }
    end
  end

  describe "GET #index" do
    context "when is successfully show" do
      before(:each) do
        @event = FactoryGirl.create :event
        @user = JSON.parse(@event.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :index
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:name]).to eql @event[:name]
      end

      it { should respond_with 200 }
    end
  end

  describe "GET #going" do
    context "when is successfully show" do
      before(:each) do
        @event = FactoryGirl.build :event
        @user = JSON.parse(@event.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        @event.sport_id = @sport.id
        @event.user_id = @user.id
        @event.save

        @attendant = Attendant.create user_id: @user.id, event_id: @event.id, attend: "going"

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :going
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:name]).to eql @event[:name]
      end

      it { should respond_with 200 }
    end
  end
end
