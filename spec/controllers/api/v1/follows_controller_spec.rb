require 'spec_helper'

describe Api::V1::FollowsController do
  describe "POST #create" do
    context "follow successfully" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        post :create, { :username => @user2.username }
      end
      it "ensure follow successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey

        post :create, { :username => @user2.username }
      end

      it { should respond_with 401 }
    end

    context "follow before" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user
        @follow = Follow.create :follower_id => @user.id, :following_id => @user2.id

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        post :create, { :username => @user2.username }
      end
      it "ensure follow successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql false
      end
      it { should respond_with 422 }
    end
  end

  describe "DELETE #destroy" do
    context "unfollow successfully" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user
        @follow = Follow.create :follower_id => @user.id, :following_id => @user2.id

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        delete :destroy, { :username => @user2.username }
      end
      it "ensure unfollow successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user
        @follow = Follow.create :follower_id => @user.id, :following_id => @user2.id

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey

        delete :destroy, { :username => @user2.username }
      end

      it { should respond_with 401 }
    end
  end

  describe "GET #followers" do
    context "list followers successfully" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user
        @follow = Follow.create :follower_id => @user.id, :following_id => @user2.id

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        get :followers, { :username => @user2.username }
      end
      it "ensure get followers successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:follower][:username]).to eql @user.username
      end
      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user
        @follow = Follow.create :follower_id => @user.id, :following_id => @user2.id

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey

        get :followers, { :username => @user2.username }
      end

      it { should respond_with 401 }
    end
  end

  describe "GET #following" do
    context "list following successfully" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user
        @follow = Follow.create :follower_id => @user.id, :following_id => @user2.id

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        get :following, { :username => @user.username }
      end
      it "ensure get following successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:following][:username]).to eql @user2.username
      end
      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @user = FactoryGirl.create :user
        @user2 = FactoryGirl.create :user
        @follow = Follow.create :follower_id => @user.id, :following_id => @user2.id

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey

        get :following, { :username => @user.username }
      end

      it { should respond_with 401 }
    end
  end
end
