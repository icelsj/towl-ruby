require 'spec_helper'

describe Api::V1::MobilePhonesController do
  describe "GET #verifyPIN" do
    context "successful" do
      before (:each) do
        @mobile = FactoryGirl.create :mobile_phone
        @mobile = MobilePhone.find(@mobile.id)

        request.headers['Accept'] = "application/json"
        request.headers['X-Check-Key'] = "12345"
        request.headers['X-Sum-Key'] = "1f39f9a1392429e163a9de7e5e26ddcf"
        get :verifyPin, { :mobile_no => @mobile.mobileno, :pin => @mobile.pin }
      end

      it { should respond_with 200 }
    end

    context "expired" do
      before (:each) do
        @mobile = FactoryGirl.create :mobile_phone
        @mobile = MobilePhone.find(@mobile.id)
        @mobile.update(:expired_at => Time.now - 30 * 60)
        request.headers['Accept'] = "application/json"
        request.headers['X-Check-Key'] = "12345"
        request.headers['X-Sum-Key'] = "1f39f9a1392429e163a9de7e5e26ddcf"
        get :verifyPin, { :mobile_no => @mobile.mobileno, :pin => @mobile.pin }
      end

      it { should respond_with 422 }
    end

    context "invalid pin" do
      before (:each) do
        @mobile = FactoryGirl.create :mobile_phone
        @mobile = MobilePhone.find(@mobile.id)
        @mobile.update(:expired_at => Time.now - 30 * 60)

        request.headers['Accept'] = "application/json"
        request.headers['X-Check-Key'] = "12345"
        request.headers['X-Sum-Key'] = "1f39f9a1392429e163a9de7e5e26ddcf"
        get :verifyPin, { :mobile_no => @mobile.mobileno, :pin => '123' }
      end

      it { should respond_with 422 }
    end
  end
end
