require 'spec_helper'

describe Api::V1::GroupsController do
  describe "GET #index" do
    context "get group list annonymously" do
      before(:each) do
        @group = FactoryGirl.create :group

        request.headers['Accept'] = "application/json"
        get :index
      end

      it "returns the list message" do
        # raise response.body
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:data][0][:name]).to eql @group.name
        expect(response_msg[:data][0].keys).to_not include(:joined)
      end

      it { should respond_with 200 }
    end

    context "get group list with member" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])
        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :index
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:data][0][:name]).to eql @group.name
        expect(response_msg[:data][0].keys).to include(:joined)
      end

      it { should respond_with 200 }
    end
  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        @group = FactoryGirl.attributes_for :group
        @user = FactoryGirl.create :user
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :create, { group: @group.merge({ sport_id: @sport[:id] }) }
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][:name]).to eql @group[:name]
      end

      it { should respond_with 201 }
    end

    context "when is not authorized" do
      before(:each) do
        @group = FactoryGirl.attributes_for :group
        request.headers['Accept'] = "application/json"
        post :create, { group: @group }, format: :json
      end

      it { should respond_with 401 }
    end

    context "when is not created" do
      before(:each) do
        @group = FactoryGirl.attributes_for :group
        @user = FactoryGirl.create :user
        @user = User.find_by_username(@user[:username])
        @sport = FactoryGirl.create :sport

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :create, { group: @group }
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql false
      end

      it { should respond_with 422 }
    end
  end

  describe "GET #show" do
    context "when is successfully shown" do
      before(:each) do
        @group = FactoryGirl.create :group
        request.headers['Accept'] = "application/json"
        get :show, id: @group.id
      end

      it "returns the not found message" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:data][:name]).to eql @group.name
      end

      it { should respond_with 200 }
    end
  end

  describe "PATCH/PUT #update" do
    context "when is successfully updated" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        put :update, { format: 'json', id: @group.id, group: { name: "Test Name" } }, format: :json
      end

      it "returns the information about a reporter on a hash" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:success]).to eql true
      end

      it { should respond_with 200 }
    end

    context "when is not authorized" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])

        request.headers['Accept'] = "application/json"
        put :update, { format: 'json', id: @user.username, group: { name: "Test Name" } }, format: :json
      end

      it { should respond_with 401 }
    end

    context "when is failed to update" do
      before(:each) do
        @group = FactoryGirl.create :group
        @user = JSON.parse(@group.user.to_json, symbolize_names: true)
        @user = User.find_by_username(@user[:username])

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        put :update, { format: 'json', id: @group.id, group: { name: " " } }, format: :json
      end

      it { should respond_with 422 }
    end
  end

  describe "GET #joined" do
    context "get group list with member" do
      before(:each) do
        # @group = FactoryGirl.create :group
        @membership = FactoryGirl.create :membership
        @group = JSON.parse(@membership.group.to_json, symbolize_names: true)
        @group = Group.find(@group[:id])
        @user = JSON.parse(@membership.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :joined
      end

      it "returns the list message" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:data][0][:name]).to eql @group.name
        expect(response_msg[:data][0].keys).to include(:joined)
      end

      it { should respond_with 200 }
    end

    context "not authorized" do
      before(:each) do
        @membership = FactoryGirl.create :membership
        @group = JSON.parse(@membership.group.to_json, symbolize_names: true)
        @group = Group.find(@group[:id])
        @user = JSON.parse(@membership.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])
        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey
        get :joined
      end

      it { should respond_with 401 }
    end
  end
end
