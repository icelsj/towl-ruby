require 'spec_helper'

describe Api::V1::CommentsController do
  describe "POST #create" do
    context "comment successfully" do
      before(:each) do
        @post = FactoryGirl.create :post, :userpost

        @comment = FactoryGirl.attributes_for :comment

        @user = JSON.parse(@post.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        post :create, { comment: @comment, post_id: @post.id }
      end
      it "ensure post successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @post = FactoryGirl.create :post, :userpost

        @comment = FactoryGirl.attributes_for :comment

        @user = JSON.parse(@post.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])

        request.headers['Accept'] = "application/json"
        # request.headers['X-Access-Key'] = @user.accesskey

        post :create, { comment: @comment, post_id: @post.id }
      end

      it { should respond_with 401 }
    end
  end

  describe "DELETE #destroy" do
    context "hide post comments successfully" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')
        @post[:targetid] = @group.id
        @post[:sport_id] = @sport.id
        @post[:user_id] = @user.id
        @post1 = Post.create(@post)

        @comment = FactoryGirl.attributes_for :comment
        @comment[:post_id] = @post1.id
        @comment[:user_id] = @user.id
        @comment = Comment.create(@comment)

        # raise RuntimeError.new @comment.to_json

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey

        delete :destroy, { :id => @comment.id }
      end
      it "ensure hide post comments successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "hide post comments successfully - Admin" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')
        @post[:targetid] = @group.id
        @post[:sport_id] = @sport.id
        @post[:user_id] = @user.id
        @post1 = Post.create(@post)

        @comment = FactoryGirl.attributes_for :comment
        @comment[:post_id] = @post1.id
        @comment[:user_id] = @user.id
        @comment = Comment.create(@comment)

        @user2 = FactoryGirl.create :user
        @user2 = User.find(@user2[:id])
        @membership2 = Membership.create(:user_id => @user2.id, :group_id => @group.id, :role => 'moderator', :status => 'joined')

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user2.accesskey

        delete :destroy, { :id => @comment.id }
      end
      it "ensure hide post comments successfully" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
      end
      it { should respond_with 201 }
    end

    context "unauthorized" do
      before(:each) do
        @post = FactoryGirl.attributes_for :post, :grouppost
        @sport = FactoryGirl.create :sport
        @group = FactoryGirl.create :group
        @user = FactoryGirl.create :user
        @user = User.find(@user[:id])
        @membership = Membership.create(:user_id => @user.id, :group_id => @group.id, :role => 'member', :status => 'joined')
        @post[:targetid] = @group.id
        @post[:sport_id] = @sport.id
        @post[:user_id] = @user.id
        @post1 = Post.create(@post)

        @comment = FactoryGirl.attributes_for :comment
        @comment[:post_id] = @post1.id
        @comment[:user_id] = @user.id
        @comment = Comment.create(@comment)

        request.headers['Accept'] = "application/json"

        delete :destroy, { :id => @comment.id }
      end

      it { should respond_with 401 }
    end
  end

  describe "GET #postcomments" do
    context "list successfully" do
      before(:each) do
        @post = FactoryGirl.create :post, :userpost
        @user = JSON.parse(@post.user.to_json, symbolize_names: true)
        @user = User.find(@user[:id])

        @post.targetid = @user.username
        @post.save

        @comment = FactoryGirl.attributes_for :comment
        @comment[:post_id] = @post.id
        @comment[:user_id] = @user.id
        @comment = Comment.create(@comment)

        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        get :postcomments, { post_id: @post.id }
      end

      it "ensure valid" do
        response_msg = JSON.parse(response.body, symbolize_names: true)
        expect(response_msg[:success]).to eql true
        expect(response_msg[:data][0][:id]). to eql @comment.id
      end

      it { should respond_with 200}
    end
  end
end
