require 'spec_helper'

describe Api::V1::SportsController do
  describe "GET #index" do
    before do
      get :index, format: :json
    end

    it "returns the information about a reporter on a hash" do
      sport_response = JSON.parse(response.body, symbolize_names: true)
      expect(sport_response[:name]) != " "
    end

    it { should respond_with 200 }
  end

  describe "GET #usersports" do
    context "when is listed successfully" do
      before(:each) do
        @user = FactoryGirl.create :user
        request.headers['Accept'] = "application/json"
        get :usersports, { username: @user.username }
      end

      it "returns the information about a reporter on a hash" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:success]).to eql true
      end

      it { should respond_with 200 }
    end

    context "when is not authorized" do
      before(:each) do
        request.headers['Accept'] = "application/json"
        get :usersports, { username: "notme" }
      end

      it "returns the information about a reporter on a hash" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:success]).to eql false
      end

      it { should respond_with 401 }
    end
  end

  describe "POST #likesports" do
    context "when is updated successfully" do
      before(:each) do
        @user = FactoryGirl.create :user
        @sport = FactoryGirl.create :sport
        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        post :likesports, { username: @user.username, sports: [@sport.id] }
      end

      it "returns the information about a reporter on a hash" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:success]).to eql true
      end

      it { should respond_with 200 }
    end

    context "when is not authorized" do
      before(:each) do
        @sport = FactoryGirl.create :sport
        request.headers['Accept'] = "application/json"
        post :likesports, { username: "sample", sports: [@sport.id] }
      end

      it { should respond_with 401 }
    end
  end

  describe "DELETE #unlikesports" do
    context "when is updated successfully" do
      before(:each) do
        @user = FactoryGirl.create :user
        @sport = FactoryGirl.create :sport
        request.headers['Accept'] = "application/json"
        request.headers['X-Access-Key'] = @user.accesskey
        delete :unlikesports, { username: @user.username, sports: [@sport.id] }
      end

      it "returns the information about a reporter on a hash" do
        user_response = JSON.parse(response.body, symbolize_names: true)
        expect(user_response[:success]).to eql true
      end

      it { should respond_with 200 }
    end

    context "when is not authorized" do
      before(:each) do
        @sport = FactoryGirl.create :sport
        request.headers['Accept'] = "application/json"
        delete :unlikesports, { username: "sample", sports: [@sport.id] }
      end

      it { should respond_with 401 }
    end
  end
end
