require 'spec_helper'

describe SportsController do
  describe "GET #index" do
    before do
      get :index, format: :json
    end
    it { should respond_with 200 }
  end

  describe "POST #create" do
    context "when is successfully created" do
      before(:each) do
        @sport_attributes = FactoryGirl.attributes_for :sport
        post :create, { sport: @sport_attributes }, format: :json
      end

      it { should respond_with 302 }
      it { should redirect_to :action => :show, :id => assigns(:sport) }
    end

    context "when is not created" do
      before(:each) do
        @invalid_sports_attributes = { name: " ",
                                     detail: " " }
        post :create, { sport: @invalid_sports_attributes }, format: :json
      end

      it { should respond_with 200 }
      it { should_not redirect_to :action => :show, :id => assigns(:sport) }
    end
  end

  describe "PUT/PATCH #update" do
    context "when is successfully updated" do
      before(:each) do
        @sport = FactoryGirl.create :sport
        patch :update, { id: @sport.id, sport: { name: "Basketball", detail: "a new description" } }, format: :json
      end

      it { should respond_with 302 }
    end

    context "when is not updated" do
      before(:each) do
        @sport = FactoryGirl.create :sport
        patch :update, { id: @sport.id, sport: { name: " ", detail: " " } }, format: :json
      end

      it { should respond_with 200 }
      it { should_not redirect_to :action => :show, :id => assigns(:sport) }
    end
  end

  describe "DELETE #destroy" do
    before(:each) do
      @sport = FactoryGirl.create :sport
      delete :destroy, { id: @sport.id }, format: :json
    end

    it { should respond_with 302 }
    it { should redirect_to :action => :index }
  end
end
