require 'spec_helper'

describe Sport do
  before { @sport = FactoryGirl.build(:sport) }

  subject { @sport }

  context 'validations' do
    it { should respond_to(:name) }
    it { should respond_to(:detail) }
  end

  it { should be_valid }

  it { should validate_presence_of :name }
  it { should validate_presence_of :detail }
end
