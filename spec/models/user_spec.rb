require 'spec_helper'

describe User do
  before { @user = FactoryGirl.build(:user) }

  subject { @user }

  context 'validations' do
    it { should respond_to(:full_name) }
    it { should respond_to(:last_name) }
    it { should respond_to(:first_name) }
    it { should respond_to(:mobileno) }
    # it { should respond_to(:email) }
    it { should respond_to(:username) }
  end

  it { should be_valid }

  it { should validate_presence_of :mobileno }
  it { should validate_length_of(:mobileno).is_at_most(20) }
  it { should validate_uniqueness_of(:mobileno).case_insensitive }

  it { should validate_length_of(:first_name).is_at_most(50) }

  it { should validate_length_of(:last_name).is_at_most(50) }

  it { should validate_presence_of :full_name }
  it { should validate_length_of(:full_name).is_at_most(100) }

  it { should validate_presence_of :username }
  it { should validate_length_of(:username).is_at_least(3).is_at_most(50) }

  # it { should validate_presence_of :email }
  # it { should validate_length_of(:email).is_at_most(255) }
  # it { should validate_format_of(:email).with(/\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i) }

end
