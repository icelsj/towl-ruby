require 'spec_helper'

describe Group do
  before { @group = FactoryGirl.build(:group) }

  subject { @group }

  context 'validations' do
    it { should respond_to(:name) }
    it { should respond_to(:detail) }
    it { should respond_to(:group_type) }
    it { should respond_to(:privacy) }
  end

  it { should be_valid }


  it { should belong_to :sport }
  it { should belong_to :user }
  it { should have_many :memberships }

  it { should validate_presence_of :name }
  it { should validate_length_of(:name).is_at_least(2).is_at_most(100) }

  it { should validate_presence_of :sport_id }
  it { should validate_presence_of :user_id }
  it { should validate_presence_of :group_type }
  it { should validate_presence_of :privacy }
end
