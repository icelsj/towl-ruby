require 'spec_helper'

describe Membership do
  before { @membership = FactoryGirl.build(:membership) }

  subject { @membership }

  context 'validations' do
    it { should respond_to(:role) }
    it { should respond_to(:status) }
  end

  it { should be_valid }

  it { should belong_to :user }
  it { should belong_to :group }

  it { should validate_presence_of :role }
  it { should validate_presence_of :status }
end
