require 'spec_helper'

describe Event do
  before { @event = FactoryGirl.build(:event) }

  subject { @event }

  context 'validations' do
    it { should respond_to(:name) }
    it { should respond_to(:description) }
    it { should respond_to(:venue) }
    it { should respond_to(:eventtime) }
    it { should respond_to(:privacy) }
  end

  it { should be_valid }

  it { should belong_to :user }
  it { should belong_to :sport }
  it { should have_many :attendant }

  it { should validate_presence_of :name }
  it { should validate_length_of(:name).is_at_least(3) }
  it { should validate_presence_of :sport_id }
  it { should validate_presence_of :user_id }
  it { should validate_presence_of :venue }
  it { should validate_presence_of :eventtime }
  it { should validate_presence_of :privacy }
end
