require 'spec_helper'

describe Comment do
  before { @comment = FactoryGirl.build(:comment) }

  subject { @comment }

  context 'validations' do
    it { should respond_to(:message) }
  end

  it { should be_valid }

  it { should belong_to :user }
  it { should belong_to :post }

  it { should validate_presence_of :message }
end
