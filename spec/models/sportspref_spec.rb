require 'spec_helper'

describe Sportspref do
  before { @sportspref = FactoryGirl.build(:sportspref) }

  subject { @sportspref }

  it { should be_valid }

  it { should belong_to :user }
  it { should belong_to :sport }
end
