require 'spec_helper'

describe Post do
  before { @post = FactoryGirl.build(:post, :grouppost) }

  subject { @post }

  context 'validations' do
    it { should respond_to(:message) }
    it { should respond_to(:posttype) }
    it { should respond_to(:targettype) }
    it { should respond_to(:targetid) }
  end

  it { should be_valid }

  it { should belong_to :user }
  it { should belong_to :sport }

  it { should validate_presence_of :message }
  it { should validate_presence_of :posttype }
  it { should validate_presence_of :targettype }
  it { should validate_presence_of :targetid }
end
