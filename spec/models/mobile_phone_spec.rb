require 'spec_helper'

describe MobilePhone do
  before { @mobile_phone = FactoryGirl.build(:mobile_phone) }

  subject { @mobile_phone }

  context 'validations' do
    it { should respond_to(:mobileno) }
    it { should respond_to(:pin) }
    it { should respond_to(:expired_at) }
    it { should respond_to(:verified) }
  end

  it { should be_valid }

  it { should validate_presence_of :mobileno }
  # it { should validate_presence_of :pin }
  # it { should validate_presence_of :expired_at }
  # it { should validate_inclusion_of(:verified).in_array([true, false])  }

  it { expect{ @mobile_phone.save }.to change{ @mobile_phone.pin } }
  it { expect{ @mobile_phone.save }.to change{ @mobile_phone.expired_at } }
end
