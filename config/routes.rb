require 'api_constraints'

Rails.application.routes.draw do
  # OmniAuth
  # match '/auth/:provider/callback', :to => 'sessions#createprovider', :via => [:get]
  # match '/auth/failure', :to => 'sessions#failure', :via => [:get, :post]

  # root and direct set
  root 'pages#index'
  get '' => 'pages#index'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  get 'admin_signup' => 'cmsusers#new'
  post 'admin_signup' => 'cmsusers#create'

  # controller resources
  resources :sports
  resources :groups

  # for api controller
  get '/api' => redirect('/swagger/dist/index.html?url=/apidocs/api-docs.json')

  # API
  namespace :api, defaults: { format: :json } do
    namespace :v1,
              constraints: ApiConstraints.new(version: 1, default: true) do
      # Users
      resources :users, param: :username, only: [:create, :show, :update]
      post 'users/:uid/login' => 'users#fblogin', as: :fblogin
      post 'users/login' => 'users#login', as: :login

      get 'users/:username/avatar' => 'users#getavatar', as: :getavatar
      post 'users/:username/avatar' => 'users#postavatar', as: :postavatar

      get 'users/:username/cover' => 'users#getcover', as: :getcover
      post 'users/:username/cover' => 'users#postcover', as: :postcover

      post 'username/change/:username' => 'users#changeusername'

      # Follows
      post 'follows/:username' => 'follows#create', as: :follow
      delete 'follows/:username' => 'follows#destroy', as: :unfollow
      get 'users/:username/followers' => 'follows#followers', as: :followers
      get 'users/:username/following' => 'follows#following', as: :following

      # Sports
      resources :sports, only: [:index]
      get 'sports/:username/preference' => 'sports#usersports', as: :usersports
      post 'sports/:username/preference' => 'sports#likesports', as: :likesports
      delete 'sports/:username/preference' => 'sports#unlikesports', as: :unlikesports

      # Groups
      resources :groups, only: [:index, :create, :show, :update]
      get 'groups/page/:page' => 'groups#index'
      get 'groups/sports/:sport_id' => 'groups#index'
      get 'groups/sports/:sport_id/page/:page' => 'groups#index'
      post 'groups/join' => 'memberships#create'
      post 'groups/leave' => 'memberships#destroy'
      get 'groups/:id/requests' => 'memberships#getrequest'
      post 'groups/:id/requests/:request_id/:action_name' => 'memberships#approverequest'
      get 'users/groups/joined' => 'groups#joined'
      get 'users/groups/joined/page/:page' => 'groups#joined'
      get 'users/groups/joined/sports/:sport_id' => 'groups#joined'
      get 'users/groups/joined/sports/:sport_id/page/:page' => 'groups#joined'
      post 'memberships/:id/promote' => 'memberships#promote'
      post 'memberships/:id/demote' => 'memberships#demote'
      post 'memberships/:id/kick' => 'memberships#kick'
      
      # Posts
      resources :posts, only: [:create, :destroy]
      get 'groups/:group_id/posts' => 'posts#listgrouppost'
      get 'groups/:group_id/posts/page/:page' => 'posts#listgrouppost'
      get 'users/:username/posts' => 'posts#listuserpost'
      get 'users/:username/posts/page/:page' => 'posts#listuserpost'
      get 'feeds' => 'posts#listfeed'
      get 'feeds/page/:page' => 'posts#listfeed'

      # Comments
      resources :comments, only: [:destroy]
      post 'posts/:post_id/comments' => 'comments#create'
      get 'posts/:post_id/comments' => 'comments#postcomments'
      get 'posts/:post_id/comments/page/:page' => 'comments#postcomments'

      # Question
      resources :answeredquestions, only: [:create, :update], param: :questionkey

      # Phone Authentication
      get 'verifymobile/:mobile_no' => 'mobile_phones#createPin'
      get 'verifymobile/:mobile_no/:pin' => 'mobile_phones#verifyPin'

      # Events
      resources :events, only: [:create, :show, :update]
      post 'events/:id/:action_name' => 'attendants#invitation_response'
      get 'events' => 'events#index'
      get 'events/page/:page' => 'events#index'
      get 'events/sports/:sport_id' => 'events#index'
      get 'events/sports/:sport_id/page/:page' => 'events#index'
      get 'users/events/going' => 'events#going'
      get 'users/events/going/page/:page' => 'events#going'
      get 'users/events/going/sports/:sport_id' => 'events#going'
      get 'users/events/going/sports/:sport_id/page/:page' => 'events#going'

      # Contacts
      post 'contactlists' => 'contactlist#searchfriends'
    end
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
